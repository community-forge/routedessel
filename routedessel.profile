<?php

use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Render\Markup;
use Drupal\Core\Access\AccessResult;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

const FICHES_PRACTIQUES_NID = 25179; //oops this node didn't migrate...
const RID_ADHERENT = 'adherent';
const RID_BENEVOLE = 'benevole';
const TID_FICHES_PRACTIQUES = 273;

ini_set('display_errors', 1);
include '../../my_platform.inc';

function is_drush() {
 return strpos($_SERVER['PHP_SELF'], 'drush');
}

/**
 * Implements hook_entity_field_access().
 */
function routedessel_entity_field_access($op, $field_definition, $account, $items = NULL){
  // Any field called 'notes' is only visible to benevoles
  $fieldname = $field_definition->getName();
  if ($fieldname == 'notes') {
    if ($op == 'view' or $op == 'edit') {
      return AccessResult::forbiddenIf(!$account->hasPermission('administer nodes'), 'No permission to administer nodes');
    }
    return AccessResult::forbidden("Not permitted to $op notes field.");
  }
  elseif ($fieldname == 'action_benevole') {
    if ($op == 'view') {
      return AccessResult::allowed();
    }
    elseif ($op == 'edit' and $items and $items->getEntity()->hasRole('benevole')) {
      return AccessResult::allowedIfHasPermission($account, 'administer nodes');
    }
    return AccessResult::forbidden('action_benevole field does not apply here.');
  }
  // These fields can only be seen by benevoles and their own users.
  if ($items) {
    $entity = $items->getEntity();
    $fields = ['address', 'edition'];
    if ($entity->getEntityTypeId() == 'user' and in_array($items->getName(), $fields)) {
      if ($account->id() <> $entity->id() and !$account->hasPermission('administer nodes')) {
        return AccessResult::forbidden('Only benevoles can use field '.$items->getName());
      }
    }
  }
  return Drupal\Core\Access\AccessResult::neutral();
}

/**
 * Implements hook_field_widget_single_element_WIDGET_TYPE_form_alter().
 */
function routedessel_field_widget_single_element_address_default_form_alter(&$element, $form_state, $context) {
  $element['#pre_render'][] = ['\Drupal\routedessel\TrustedCallbacks', 'removeCountryReunion'];
  // I don't know why this is needed. @see template_preprocess_details and Drupal\Core\Render\Element\Details::preRenderDetails
  $element['#attributes']['open'] = $element['#open'];
}


/**
 * Implements hook_form_FORM_ID_alter();
 */
function routedessel_form_user_form_alter(&$form, $form_state) {
  // Hide the username field
  $form['account']['name']['#access'] = FALSE;
  // Turn off Current password field's validation
  $form_state->set('user_pass_reset', 1);
  // Hide the Current password fields
  $form['account']['current_pass']['#access'] = FALSE;
  // Make the two phone fields less ugly - better done with css?
  if ($form_state->get('form_display')->getComponent('phones')) {
    // Remove the multiple field theme from the phone numbers.
    $form['phones']['widget'][0]['_weight']['#access'] = FALSE;
    $form['phones']['widget'][1]['_weight']['#access'] = FALSE;
    unset($form['phones']['widget']['#theme']);
    $form['phones']['widget'][0]['#prefix'] = 'Phone 1';
    $form['phones']['widget'][1]['#prefix'] = 'Phone 2';
    $form['phones']['widget'][0]['#required'] = TRUE;
  }
  $form['contact']['contact']['#access'] = FALSE;

  // This callback has to go first or username validation will fail early on.
  array_unshift($form['#validate'], 'routedessel_user_form_settings_validate');
}
/**
 * Form validation callback.
 * Populate the username field with the email to ensure it is unique.
 */
function routedessel_user_form_settings_validate($form, $form_state) {
  // populate the user name field with the email which is unique.
  $form_state->setValue('name', $form_state->getValue('mail'));
}

/**
 * Implements hook_entity_extra_field_info().
 */
function routedessel_entity_extra_field_info() {
  $extrafields = [
    'user' => [
      'user' => [
        'display' => [
          'contact' => [
            'label' => 'Contact link',
            'description' => "To the user's contact form",
            'visible' => TRUE,
            'weight' => 10
          ]
        ]
      ]
    ]
  ];
  $nodebundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo('node');
  foreach (array_keys($nodebundles) as $bundle) {
    $extrafields['node'][$bundle]['form']['help'] = [
      'label' => 'Help',
      'description' => 'Advice to users creating a new node',
      'visible' => TRUE,
      'weight' => -1
    ];
  }
  return $extrafields;
}


/**
 * Implements hook_form_FORM_ID_alter();
 */
function routedessel_form_node_form_alter(&$form, $form_state) {
  $display = $form_state->get('form_display');
  $node = $form_state->getFormObject()->getEntity();
  $bundle = $node->bundle();
  if ($info = $display->getComponent('help')) {
    $form['help'] = [
      '#type' => '#markup',
      '#weight' => $info['weight'],
      '#markup' => NodeType::load($bundle)->getHelp()
    ];
  }
  $form['revision_log']['#access'] = FALSE;

  if ($bundle == 'document' and $node->doc_categories->target_id == TID_FICHES_PRACTIQUES) {
    $form['target_path'] = [
      '#title' => 'Target page',
      '#type' => 'title_target',
      '#keystore' => 'fiche_practique:'.$node->id(),
      '#placeholder' => 'user/*/transactions',
      '#required' => TRUE,
      '#weight' => -2,
    ];
    $form['actions']['submit']['#submit'][] = ['Drupal\routedessel\Element\TitleTarget', 'submit'];
  }
  // I don't know where vertical tabs were removed.
  $form['advanced']['#type'] = 'vertical_tabs';
  $form['advanced']['#weight'] = 99;
}

/**
 * Theme preprocessor for page title
 * Append the fichepractique link after the title if needed.
 */
function routedessel_preprocess_page_title(&$variables) {
  $request = \Drupal::requestStack()->getCurrentRequest();
  $path = \Drupal::service('path.current')->getPath($request);
  foreach (\Drupal::keyValue('fiche_practique')->getAll() as $nid => $pattern) {
    if (\Drupal::service('path.matcher')->matchPath($path, '/'.$pattern)) {
      // Load the node and if it has an attachment link to that, otherwise to the node.
      // We know that the node is a 'document'
      $node = Node::load($nid);
      $link_options = ['attributes' => ['target' => '_blank', 'title' => $node->label()]];
      $uri = $node->upload_private->isEmpty() ?
        '/node/'.$nid :
        $node->upload_private->entity->createFileUrl();
      $link = Link::fromTextAndUrl(
        Markup::create('<span class="fichepractique"></span>'),
        Url::fromUri("internal:$uri", $link_options)
      );
      $variables['fichepratique'] = $link->toString();
      break;
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_view().
 */
function routedessel_user_view(array &$build, $account, $display, $view_mode) {
  if ($conf = $display->getComponent('contact')) {
    $build['contact'] = [
      '#type' => 'link',
      '#title' => 'Contactez',
      '#url' => Drupal\Core\Url::fromRoute('entity.user.contact_form', ['user' => $account->id()]),
      '#weight' => $conf['weight']
    ];
  }
  // Link the full name in the compact mode.
  if ($display->getMode() == 'compact' and $conf = $display->getComponent('fullname')) {
    $build['fullname'] = $account->toLink()->toRenderable();
  }
}

/**
 * Implements hook_local_tasks_alter().
 */
function routedessel_local_tasks_alter(&$tasks) {
  $tasks['entity.webform_submission.user']['title'] = 'Reponses';
  $tasks['entity.user.edit_form']['weight'] = 2;
  $tasks['entity.webform_submission.user']['weight'] = 12; // can hide if empty
  $tasks['entity.user.contact_form']['weight'] = 14; // can't see your own.
  unset(
    $tasks['entity.webform_submission.user']
  );
}

/**
 * Implements hook_block_view_BASE_BLOCK_ID_alter().
 *
 * Change the title of the user block.
 */
function routedessel_block_view_system_menu_block_alter(&$build, $block) {
  if ($block->getPluginId() == 'system_menu_block:account') {
    $build['#cache']['contexts'][] = 'user';
    $build['#configuration']['label'] = \Drupal::currentUser()->getDisplayName();
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function routedessel_form_masquerade_block_form_alter(&$form, $form_state) {
  $form['autocomplete']['masquerade_as']['#selection_handler'] = 'fullname:user';
}

/**
 * Implements hook_file_download().
 */
function routedessel_file_download($uri) {
  if (strpos($uri, 'anonymous.png')){
    return ['foo' => 'bar'];
  }
  // This means only admins and people who have paid.
  if (\Drupal::currentUser()->hasPermission('access user profiles')) {
    // Return any http header to grant access to authenticated users.
    return ['foo' => 'bar'];
  }
}

/**
 * Implements hook_menu_links_discovered_alter().
 */
function routedessel_menu_links_discovered_alter(&$links) {
  unset($links['admin_toolbar_tools.extra_links:user.admin_create']);
}

/**
 * Implements hook_mail_alter();
 * Add the Route des SEL mail footer
 */
function routedessel_mail_alter(&$message) {
  $message['body'][] = \Drupal::config('routedessel.settings')->get('mail_footer');
}

/**
 * Implements hook_entity_type_build().
 * Hide all node revisioning.
 */
function routedessel_entity_type_build(&$entity_types) {
  //if (isset($entity_types['node'])) {
    $entity_types['node']->set('show_revision_ui', FALSE);
  //}
}

/**
 * Implements hook_block_view_BLOCK_ID_alter().
 * Add a prerender callback to add another link to the login block.
 */
function routedessel_block_view_user_login_block_alter(&$build, $block) {
  $build['#pre_render'][] = ['\Drupal\routedessel\TrustedCallbacks', 'loginBlockAddForgotLink'];
}

/**
 * Implements theme hook_preprocess page().
 * Add the stylesheet to every page, because it has miscellaneous stuff.
 */
function routedessel_preprocess_page(&$vars) {
  $vars['#attached']['library'][] = 'routedessel/routedessel-styling';
}

/**
 * Utility
 * Calculate a meaningful, hopefully unique name for each user, based on their fullname and SEL
 *
 * @param \Drupal\user\UserInterface $user
 * @return string
 */
function routedessel_unique_name(\Drupal\user\UserInterface $user) : string {
  if (\Drupal::currentUser()->isAnonymous()) {
    $account_switcher = \Drupal::service('account_switcher');
    $account_switcher->switchTo(User::load(1));
    $nameAndSEL = $user->label();
    $account_switcher->switchBack(1);
  }
  else {
    $nameAndSEL = $user->label();
  }
  if ($sel_names = get_sels_of_user($user)) {
    $nameAndSEL .= ' de ' . reset($sel_names);
  }
  else {
    $nameAndSEL .= ' (Sans SEL)';
  }
  return $nameAndSEL;
}

/**
 * Implements hook_views_pre_render().
 * Set the title of all views with a user argument
 */
function routedessel_views_pre_render(Drupal\views\ViewExecutable $view) {
  if (isset($view->argument['uid'])) {
    $pos = array_search('uid', $view->argument);
    $wid = $view->args[$pos];
    $view->setTitle(User::load($wid)->label());
  }
}

/**
 * Implements hook_user_login().
 * Redirect user on login.
 */
function routedessel_user_login($account) {
  $current_request = Drupal::request();
  $destination = $current_request->query->get('destination');
  if ($destination && $destination != '/user/login' && $destination != '/accueil') {
    // Acceuil is the home page setting. I can't think of a more elegant way of getting around the
    return;
  }
  $current_route = Drupal::routeMatch()->getRouteName();
  if (!in_array($current_route, ['user.reset', 'user.reset.login'])) {
    $current_request->query->set('destination', '/nouvelles');
  }
}


/**
 * Implements hook_field_views_data_alter().
 * @temp while module views_sort_null_field isn't updated to d10
 */
function routedessel_field_views_data_alter(array &$data, \Drupal\field\FieldStorageConfigInterface $field_storage) {
  // Skip fields with custom storage, we can't work with them.
  if ($field_storage->hasCustomStorage()) {
    return;
  }

  $entity_type_id = $field_storage->getTargetEntityTypeId();
  $entity_type = \Drupal::entityTypeManager()->getDefinition($entity_type_id);
  $field_name = $field_storage->getName();
  list($label) = views_entity_field_label($entity_type_id, $field_name);
  $args = array(
    '@label' => $label,
    '@name' => $field_name,
  );
  $table_mapping = \Drupal::entityTypeManager()->getStorage($entity_type_id)->getTableMapping();
  $field_columns = $field_storage->getColumns();
  $table_alias = $entity_type_id . '__' . $field_name;

  foreach ($field_columns as $column => $attributes) {
    // Skip columns that can't be NULL: our filter is pointless for these.
    if (!empty($attributes['not null'])) {
      continue;
    }

    $args['@column'] = $column;
    $column_real_name = $table_mapping->getFieldColumnName($field_storage, $column);

    if (count($field_columns) == 1 || $column == 'value') {
      $title = t('@label (@name) null sort', $args);
      $title_short = t("@label null sort", $args);
    }
    else {
      $title = t('@label (@name:@column) null sort', $args);
      $title_short = t('@label:@column null sort', $args);
    }
    $data[$table_alias][$column_real_name . '_null_sort'] = array(
      'title' => $title,
      'title short' => $title_short,
      'group' => $entity_type->getLabel(),
      'help' => t('Sort entities with no value (NULL) last or first.'),
      'sort' => array(
        'field' => $column_real_name,
        'id' => 'null_sort',
      ),
    );
  }

}
