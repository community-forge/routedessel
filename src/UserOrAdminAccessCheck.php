<?php

namespace Drupal\routedessel;

use Symfony\Component\Routing\Route;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;

/**
 * Custom access check to allow the user and admin to access the users' stuff.
 */

class UserOrAdminAccessCheck implements AccessInterface {

  /**
   * Prevent access to specific nodes and menu items
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    if ($route_match->getParameter('user') == $account->id()) {
      return AccessResult::allowed();
    }
    return AccessResult::allowedifHasPermission($account, 'administer users');
  }
}

