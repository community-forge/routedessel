<?php

namespace Drupal\routedessel;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\give\Entity\Donation;

/**
 * Description of ExportCarnetMails
 *
 * @todo inject EntityQuery service
 * @todo inject DateFormatter service
 */
class ExportCarnetMails extends \Drupal\Core\Form\ConfigFormBase {

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state): array {
    $last_date = $this->config('routedessel.settings')->get('carnet_last_export') ?:strtotime('Jan 1');

    $form['last'] = [
      '#type' => 'item',
      '#markup' => new TranslatableMarkup(
        'Dernière exportation: :date',
        [':date' => \Drupal::service('date.formatter')->format($last_date)]
      ),
      '#weight' => 1
    ];
    $dids = $this->donationIdsSince($last_date);
    $form['num'] = [
      '#type' => 'item',
      '#markup' => new TranslatableMarkup('Payments completed since then: :num', [':num' => count($dids)]),
      '#weight' => 1
    ];
    $form['date'] = [
      '#title' => 'Export all addresses of users who paid since',
      '#type' => 'datetime',
      '#default_value' => DrupalDateTime::createFromTimestamp($last_date),
      '#date_year_range' => date('Y').':'.date('Y'),
      '#required' => TRUE,
      '#weight' => 3
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => "Get csv",
      '#weight' => 4
    ];
    return $form;
  }

  public function getFormId(): string {
    return 'export_carnet_mails';
  }

  private function donationIdsSince(int $unixtime) {
    return \Drupal::entityQuery('give_donation')->accessCheck(TRUE)
      ->condition('complete', 1)
      ->condition('changed', $unixtime, '>')
      ->execute();
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $since = $form_state->getValue('date');
    $dids = $this->donationIdsSince($last_date);
    foreach (Donation::loadMultiple($this->dids) as $donation) {
      $address = $donation->getOwner()->address->getValue();
      // make a csv file?



      $request = new Request(['file' => 'addresses.csv']);
      return $this->fileDownloadController->download($request, 'temporary');
    }
    $this->configFactory()->getEditable('routedessel.settings')
      ->set('carnet_last_export', \Drupal::time()->getRequestTime())
      ->save();
  }


  protected function getEditableConfigNames() {
    return ['routedessel.settings'];
  }

}
