<?php

namespace Drupal\routedessel\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a block.
 */
#[Block(
  id: 'gdpr_info',
  admin_label: new TranslatableMarkup('GDPR info'),
  category: new TranslatableMarkup('RdS')
)]
class GDPRInfoBlock extends BlockBase implements ContainerFactoryPluginInterface {


  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
    );
  }

  public function build(): array {
    $build['image'] = [
      '#theme' => 'image_formatter',
      '#item' => [
        'uri' => '/sites/route-des-sel.org/files/RGPD.JPG',
      ],
      '#path' => [
        'path' => '/sites/route-des-sel.org/files/Le Réglement Général sur la Protection des Données personnelles_0.pdf',
        'options' => [
          'html' => TRUE,
          'alt' => $this->t('FICHE PRATIQUE Les associations et le Règlement Général sur la Protection des Données'),
        ],
      ],
      '#weight' => 0
    ];
    $build['adh'] = [
      '#title' => 'Notre politique',
      '#type' => 'link',
      '#url' => Url::fromUri('rgpd/adherents'),
      '#weight' => 1,
      '#access' => $this->currentUser->hasRole(RID_ADHERENT)
    ];
    $build['corr'] = [
      '#title' => 'Pour notre correspondants',
      '#type' => 'link',
      '#url' => Url::fromUri('/rgpd/correspondants'),
      '#weight' => 1,
      '#access' => is_correspondent($this->currentUser)
    ];
    $build['corr_form'] = [
      '#title' => 'Formulaire à remplir obligatoirement pour ETRE ou RESTER correspondant',
      '#type' => 'link',
      '#url' => Url::fromUri('/node/25577'),// this should have an alias
      '#access' => is_correspondent($this->currentUser)
    ];
    $build['bene'] = [
      '#title' => 'Pour notre benevoles',
      '#type' => 'link',
      '#url' => Url::fromUri('/rgpd/benevoles'),
      '#access' => $this->currentUser->hasRole('benevole')
    ];
    return $build;
  }

}
