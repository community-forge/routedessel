<?php

namespace Drupal\routedessel\Plugin\views\access;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Attribute\ViewsAccess;
use Symfony\Component\Routing\Route;
use Drupal\views\Plugin\views\access\AccessPluginBase;
use Drupal\views\Attribute\ViewsAccess;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Validates for the user passed in the first argument OR admin.
 *
 * @ingroup views_access_plugins
 */
#[ViewsAccess(
  id: 'user_or_admin',
  title: new TranslatableMarkup('User or Admin'),
  help: new TranslatableMarkup("Access will be granted to user in the first argument or users with 'administer users' permission."),
)]
class UserOrAdmin extends AccessPluginBase implements CacheableDependencyInterface {

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    return $account->id() == \Drupal::routeMatch()->getParameter('user')
      or $account->hasPermission('administer users');
  }

  /**
   * {@inheritdoc}
   */
  public function alterRouteDefinition(Route $route) {
    $route->setRequirements(['_user_or_admin' => 'TRUE']);
  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    return $this->t('User or Admin');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return [];
  }

}
