<?php

namespace Drupal\routedessel\Plugin\views\argument_validator;

use Drupal;
use Drupal\user\Plugin\views\argument_validator\User;
use Drupal\views\Attribute\ViewsArgumentValidator;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Validates whether user access is allowed.
 * @note taken from https://www.drupal.org/project/views_private_access
 */
#[ViewsArgumentValidator(
  id: 'user_private',
  title: new TranslatableMarkup(),
  entity_type: 'user'
)]
class UserPrivate extends User {

  /**
   * {@inheritdoc}
   */
  public function validateEntity(Drupal\Core\Entity\EntityInterface $entity) {
    $user = \Drupal::currentUser();
    if ($user->hasPermission('administer users')) {
      return TRUE;
    }

    return parent::validateEntity($entity);
  }

}
