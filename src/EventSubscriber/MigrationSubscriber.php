<?php

namespace Drupal\routedessel\EventSubscriber;

use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Drupal\migrate\Event\MigrateImportEvent;
use Drupal\migrate\Row;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Modify migrations before saving them.
 *
 * @todo inject user_data, configFactory, Database
 */
class MigrationSubscriber implements EventSubscriberInterface {

  private $parcheminFids = [
    //parchemins nodes with attached fids
    28143 => [20691, 20690],
    28154 => [20693, 20692],
    27960 => [20695, 20694]
  ];

  const FILTER_FORMATS = [
    '2' => 'full_html',
    '1' => 'restricted_html',
    '5' => 'plain_text', // This is the fallback
  ];
  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents():array {
    return [
      'migrate.pre_row_save' => ['migratePreRowSave'],
      'migrate.post_import'=> ['migratePostImport']
    ];
  }

  function migratePreRowSave(MigratePreRowSaveEvent $event) {
    $row = $event->getRow();
    $migration = $event->getMigration();
    $source = $row->getSource();

    if ($migration->id() == 'd7_user') {
      $address = $row->getDestinationProperty('location_user');
      $address[0]['administrative_area'] = $this->convertAdminArea($address[0]['administrative_area']);
      $row->setDestinationProperty('address', $address);
      $row->setDestinationProperty('location_user', []);
      $row->setDestinationProperty('name', $row->getDestinationProperty('mail'));

      // Roles, which don't quite use the name expected by the migration system
      $roles = [
        3 => 'adherent',
        4 => 'benevole',
        9 => 'correspondant'
      ];
      $user_roles = $row->getDestinationProperty('roles');
      foreach ($row->getSourceProperty('roles') as $old_rid) {
        $user_roles[] = $roles[$old_rid];
      }
      $row->setDestinationProperty('roles', array_unique(array_filter($user_roles)));
    }
    if ($migration->id() == 'd7_field' or $migration->id() == 'd7_field_instance') {
      if ($row->getSourceProperty('field_name') == 'languages') {
        $row->setDestinationProperty('field_name', 'spoken');
      }
    }


    if ($migration->id() == 'd7_node:lodging') {
      // Convert from taxonomy reference into text list fields
      switch($row->getSourceProperty('distance')[0]['tid']) {
        case 144: $val = -4; break;
        case 145: $val = -3; break;
        case 146: $val = -2; break;
        case 147: $val = -1; break;
        case 148: $val = 0;
      }
      $transport = [['value' => $val]];
      $row->setDestinationProperty('transport', $transport);

      $old_misc = $row->getSourceProperty('misc');
      foreach ($old_misc as $delta => $item) {
        switch($item['tid']) {
          case 140: $vals[] = ['value' => 'access']; break;
          case 244: $vals[] = ['value' => 'animals']; break;
          case 141: $vals[] = ['value' => 'sheets']; break;
          case 142: $vals[] = ['value' => 'selfcatering']; break;
          case 138: $vals[] = ['value' => 'camping']; break;
          case 139: $vals[] = ['value' => 'meals']; break;
          case 143: $vals[] = ['value' => 'smoking']; break;
          case 330: $vals[] = ['value' => 'internet'];
        }
      }
      $row->setDestinationProperty('misc', $vals);

      switch($row->getSourceProperty('environment')[0]['tid']) {
        case 149: $val = 'village'; break;
        case 150: $val = 'urban'; break;
        case 151: $val = 'country';
      }
      $env = [['value' => $val]];
      $row->setDestinationProperty('environment', $env);

      if ($row->hasDestinationProperty('address')) {
        // we've change the fieldnames provided by the location_migration module
        $address = $row->getDestinationProperty('address');
        unset($address[0]['organization']);
        $row->setDestinationProperty('address', $address);
      }


      //$row->setDestinationProperty('geolocation', $row->getDestinationProperty('location_node_geoloc'));

    }

    if ($row->hasDestinationProperty('body')) {
      $this->entityBodyFilterFormat($row, 'body');
    }
    if ($row->hasDestinationProperty('comment_body')) {
      $this->entityBodyFilterFormat($row, 'comment_body');
    }

    if ($migration->id() == 'd7_node:document') {
      $row->setDestinationProperty('doc_categories', $row->getDestinationProperty('cforge_docs_categories'));
      if ($row->getDestinationProperty('nid') == 25179) {
        $row->setDestinationProperty('type', 'page');
        $row->setDestinationProperty('upload', $row->getDestinationProperty('upload_private'));
        if ($term = Term::load(273)) {
          $term->delete();
        }
      }
    }

    if ($migration->id() == 'd7_taxonomy_term:cforge_docs_categories') {
      $row->setDestinationProperty('vid', 'documents');
    }

    if ($row->getSourceProperty('field_name') == 'cforge_docs_categories') {
      $row->setDestinationProperty('field_name', 'doc_categories');
      $settings = $row->getDestinationProperty('settings');
      $settings['allowed_values'][0]['vocabulary'] = 'documents';
      $row->setDestinationProperty('settings', $settings);
    }
    if ($migration->id() == 'd7_node:parchemins') {
      // attach the files to the latest parchemins nodes.
      if ($fids = $this->parcheminFids[$row->getSourceProperty('nid')]) {
        $row->setDestinationProperty('upload', ['target_id' => $fids[0], 'display' => 1]);
        $row->setDestinationProperty('upload_video', ['target_id' => $fids[1], 'display' => 1]);
      }
    }
  }


  /**
   * Convert the filter format of a body field
   * @param Row $row
   * @param string $body_field_name
   * @param type $default
   */
  private function entityBodyFilterFormat(Row $row, $body_field_name, $default = 'restricted_html') {
    if ($bods = $row->getDestinationProperty($body_field_name)) {
      foreach ($bods as $delta => &$body) {
        $old = $body['format'] ? $body['format'] : 'restricted_html';
        $body['format'] = $this->convertFormat($old, $default);
      }
      $row->setDestinationProperty($body_field_name, $bods);
    }
  }

  /**
   * Look up the new filter format witih the old
   */
  private function convertFormat($old_format_name, $default) {
    return static::FILTER_FORMATS[$old_format_name] ?? $default;
  }


  private function recombineParChemins() {
    $node = Node::load(PAR_CHEMINS_NID);
    $node->setTitle('Par Chemins');
    foreach ($this->parcheminFids as $fid) {
      $attached[] = [
        'target_id' => $fid,
        'display' => 1
      ];
    }
    $node->upload_private->setValue($attached);
    $node->save();

    // Delete all the par chemins nodes we just imported.
    $nids = \Drupal::entityQuery('node')->accessCheck(FALSE)
      ->condition('type', 'parchemins')
      ->condition('nid', PAR_CHEMINS_NID, '<>')
      ->execute();
    foreach (Node::loadMultiple($nids) as $node) {
      $node->delete();
    }

  }

  function migratePostImport(MigrateImportEvent $event) {
    $migration_id = $event->getMigration()->id();
    if ($migration_id == 'd7_user') {
      // Because I can't see where this is supposed to happen
      \Drupal::configFactory()->getEditable('user.settings')
        ->set('register', 'visitors_admin_approval')
        ->save();
      \Drupal::database()->delete('users_data')->condition('module', 'contact')->execute();
    }
    if ($migration_id == 'd7_node:poll') {
      \Drupal::database()
        ->update('node_field_data')
        ->fields(['promote' => 0])
        ->condition('type', 'poll')
        ->execute();
    }

  }

  /**
   * Convert the regions/departments from the old format from the d7 location module to the ISO codes
   * @param string $country_code
   * @param string $old
   * @return string
   */
  static function convertAdminArea(string $country_code, string $old = NULL) {
    $data_dir = 'profiles/routedessel/content/';
    if (!in_array($country_code, ['FR', 'CH', 'BE'])) {
      return $old;
    }
    static $dictionary = [];
    if (!isset($dictionary[$country_code])) {
      $depts = json_decode(file_get_contents($data_dir."/$country_code.json"));
      switch ($country_code) {
        case 'BE':
          foreach ((array)$depts->subdivisions as $name => $info) {
            $dictionary['BE'][substr($info->iso_code, 3)] = $name;
          }
          break;
        case 'CH':
          foreach ((array)$depts->subdivisions as $name => $info) {
            $dictionary['CH'][substr($info->iso_code, 3)] = $name;
          }
          break;
        case 'FR':
          foreach ((array)$depts->subdivisions as $name => $info) {
            $dictionary['FR']['A'.$info->postal_code_pattern] = $name;
          }
          $dictionary['FR'] += [
            'A20' => 'Corse',// note this is actually 2 departments, 2A and 2B
            'C71' => 'Guadeloupe',
            'C72' => 'Martinique',
            'C73' => 'Guyane',
            'C74' => 'La Reunion'
          ];
          break;
      }
      // to use this function in another script
      if (!$old) return $dictionary[$country_code];
    }
    return $dictionary[$country_code][$old];
  }
}

