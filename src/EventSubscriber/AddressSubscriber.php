<?php

namespace Drupal\routedessel\EventSubscriber;

use Drupal\address\Event\AddressFormatEvent;
use Drupal\address\Event\AddressEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;


/**
 * Tweaks address formats and redirects language prefixes
 *
 * @todo separate this into two
 */
class AddressSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    $events[AddressEvents::ADDRESS_FORMAT] = [['rewriteAddress']];
        // This needs to run before RouterListener::onKernelRequest(), which has
    // a priority of 32. Otherwise, that aborts the request if no matching
    // route is found.
    $events[KernelEvents::REQUEST][] = ['onKernelRequestCheckRedirect', 33];
    return $events;
  }

  /**
   * Example format
   * 'FR' => [
   *   'format' => "%organization\n%givenName %familyName\n%addressLine1\n%addressLine2\n%postalCode %locality %sortingCode",
   *   'required_fields' => ['addressLine1', 'locality', 'postalCode'],
   *   'uppercase_fields' => ['locality', 'sortingCode'],
   *   'postal_code_pattern' => '\d{2} ?\d{3}',
   */
  public function rewriteAddress(AddressFormatEvent $event) {
    $addressFormat = $event->getDefinition();
    if (in_array($addressFormat['country_code'], ['FR', 'BE', 'CH'])) {
      $addressFormat['format'] = "%addressLine1\n%addressLine2\n%postalCode %locality %administrativeArea";
      $addressFormat['required_fields'][] = 'administrativeArea';
      $addressFormat['required_fields'] = array_unique($addressFormat['required_fields']);
      $addressFormat['subdivision_depth'] = 1;
      $event->setDefinition($addressFormat);
    }
  }

  /**
   * Crude redirect handler, scraped from the redirect module.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The event to process.
   */
  public function onKernelRequestCheckRedirect(RequestEvent $event) {
    $request = clone $event->getRequest();
    if (strpos($request->getPathInfo(), '/system/files/') === 0 && !$request->query->has('file')) {
      // Private files paths are split by the inbound path processor and the
      // relative file path is moved to the 'file' query string parameter. This
      // is because the route system does not allow an arbitrary amount of
      // parameters. We preserve the path as is returned by the request object.
      // @see \Drupal\system\PathProcessor\PathProcessorFiles::processInbound()
      $path = $request->getPathInfo();
    }
    else {
      $path = \Drupal::service('path_processor_manager')->processInbound($request->getPathInfo(), $request);
    }
    $path = trim($path, '/');
    $parts = explode('/', $path);
    $first = array_shift($parts);

    if ($first == 'fr' or $first == 'en') {
      $response = new TrustedRedirectResponse(
        Url::fromUserInput('/'.implode('/', $parts))->setAbsolute()->setOption('https', TRUE)->toString(),
        301
      );
      $event->setResponse($response);
    }
  }

}
