<?php

namespace Drupal\routedessel\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Alter routes.
 *
 * Change some routes into admin routes. Change some route titles.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   *
   * Anything the committee does shouldn't be an admin_route.
   */
  protected function alterRoutes(RouteCollection $collection) {
    // prevent anyone from creating accounts apart from anon.
    $collection->get('user.admin_create')->setRequirement('_access', 'FALSE');
    // unfortunately that doesn't remove the link in the admin toolbar
    $collection->get('entity.user.edit_form')->setOption('_admin_route', FALSE);

    // Make all page titles under user/ the same
    // If not overridden, this works for non-views pages
    foreach ($collection->all() as $route) {
      if (substr($route->getPath(), 0, 12) == '/user/{user}') {
        $route->setDefault('_title_callback', '\Drupal\user\Controller\UserController::userTitle');
      }
    }
    $collection->get('entity.group.edit_form')
      ->setRequirements(['_permission' => 'administer users']);

    $collection->get('entity.mc_wallet.edit_form')
      ->setRequirements(['_permission' => 'manage mcapi']);

    $collection->get('mcapi.signatures.anonsign')
      ->setDefaults([
        '_entity_form' => 'mc_transaction.transition',
        'dest_state' => 'completed'
      ]);
  }
}
