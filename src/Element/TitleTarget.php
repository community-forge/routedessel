<?php

namespace Drupal\routedessel\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Attribute\FormElement;

/**
 * Stores a path for an entity.
 */
#[FormElement('title_target')]
class TitleTarget extends \Drupal\Core\Render\Element\Textfield {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#element_validate'] = [[get_class($this), 'validate']];
    $info['#keystore'] = NULL;
    $info['#size'] = 40;
    $info['#process'] = array_merge([[get_class($this), 'process']], $info['#process']);
    return $info;
  }

  /**
   * The #value has already been set from the #default_value
   */
  public static function process(&$element, FormStateInterface $form_state, &$complete_form) {
    list($keystore, $id) = explode(':', $element['#keystore']);
    $element['#description'] = "Utiliser l'astérisque comme joker, par exemple user/*/transactions";
    $default_value = \Drupal::keyValue($keystore)->get($id);
    if (!$element['#value']) {
      $element['#value'] = $default_value;
    }
    $element['#default_value'] = $default_value;
    return $element;
  }

  /**
   * Element validate callback.
   */
  public static function validate(&$element, FormStateInterface $form_state) {
    $path = trim($element['#value']);
    if (!\Drupal::service('path.validator')->isValid($path)) {
      $form_state->setError($element, 'Invalid path (must begin with /?)');
    }
    $form_state->setValueForElement($element, $path);
  }

  /**
   * Form submit callback
   */
  public static function submit($form, $form_state) {
    // this method doesn't really belong here, so we have to locate the right element in the form.
    foreach (Element::children($form) as $key) {
      if (isset($form[$key]['#keystore'])) {
        list($keystore, $id) = explode(':', $form[$key]['#keystore']);
      }
    }
    \Drupal::keyValue($keystore)->set($id, $form_state->getValue($key));
  }

}
