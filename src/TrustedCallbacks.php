<?php

namespace Drupal\routedessel;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Url;

/**
 * Trusted callbacks
 */
class TrustedCallbacks implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['loginBlockAddForgotLink', 'blockTitleAlter', 'removeCountryReunion'];
  }

  static function loginBlockAddForgotLink($build) {
    $build['content']['user_links']['#items']['forgot all'] = [
      '#type' => 'link',
      '#title' => "Autres problèmes de connexion",
      '#url' => Url::fromRoute('entity.contact_form.canonical', ['contact_form' => 'forgot'], [
        'attributes' => [
          'title' => 'Demandez à notre équipe de récupérer vos données de connexion',
         // 'class' => ['request-password-link'],
        ],
      ]),
    ];
    $build['content']['user_login_form']['name']['#title'] = "Votre n° d'adhérent ou Courriel";
    return $build;
  }

  static function blockTitleAlter($build) {
    $mems = \Drupal::entityTypeManager()->getStorage('user')->getQuery()->accessCheck(TRUE)
      ->condition('roles', RID_ADHERENT)
      ->condition('status', 1)
      ->count()->execute();
    $lodgings = \Drupal::entityTypeManager()->getStorage('node')->getQuery()->accessCheck(TRUE)
      ->condition('type', 'lodging')
      ->condition('status', 1)
      ->condition('uid', 1, '>')// needed because ATM there's a data integrity problem
      ->count()->execute();
    $build['content']['#suffix'] = "$mems membres avec $lodgings hébergements";
    return $build;
  }


  static function removeCountryReunion($build) {
    unset($build['address']['country_code']['country_code']['#options']['RE']);
    return $build;
  }

}
