(function ($, Drupal) {
  $('input.datepicker-years-filter').each(function () {
    $(this).datepicker('destroy');
    $(this).datepicker({
      disableTouchKeyboard: true,
      dateFormat: 'yy',
      minViewMode: 'years',
      direction: "down",
    });
  });
})(jQuery, Drupal);