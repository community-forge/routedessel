<?php

namespace Drupal\rds_payment;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;

/**
 * Ask the non EU user if they intend to create a lodging.
 * @deprecated
 */
class FreeUserForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#title'] = 'Free membership?';
    $form['explanation'] = [
      '#type' => 'item',
      '#markup' => '<p>Members outside the EU who offer accommodation are entitled to free membership.</p><p>Would you like to offer accommodation?',
      '#weight' => 1
    ];
    $form['yes'] = [
      '#type' => 'submit',
      '#name' => 'yes',
      '#value' => "Yes, I will list my accommodation now",
      '#weight' => 2
    ];
    $form['no'] = [
      '#type' => 'submit',
      '#name' => 'no',
      '#value' => 'No I want to pay now',
      '#weight' => 3
    ];
    return $form;
  }
  /**
   * {@inheritdoc}
   * // Grant the adherent role to users and trigger the mail
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $clicked = $form_state->getTriggeringElement()['#name'];
    if ($clicked == 'no') {
      $form_state->setRedirect('rds_payment.payform');
    }
    elseif ($clicked == 'yes')  {
      $form_state->setRedirect('entity.node.add_form', ['entity_type' => 'lodging']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'free_user_form';
  }


}
