<?php

namespace Drupal\rds_payment;

use Drupal\give\Entity\Donation;
use Drupal\Core\Controller\ControllerBase;

/**
 * Form builder for settings.
 */
class Pages extends ControllerBase {

  function paymentForm() {
    $account = $this->currentUser();
    $payment_entity = Donation::create([
      'give_form' => 'membership',
      'name' => $account->getDisplayName() .'('.$account->id().')',
      'mail' => $account->getEmail(),
      'amount' => RDS_MEMBERSHIP_FEE,
      'recurring' => 0,
    ]);
    $form = \Drupal::service('entity.form_builder')
      ->getForm($payment_entity, 'add');

    return $form;
  }

  function myPayments() {
    return $this->redirect(
      'view.give_donations.page_user',
      ['user' => $this->currentUser()->id()]
    );
  }
}
