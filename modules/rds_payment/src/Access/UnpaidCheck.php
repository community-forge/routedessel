<?php

namespace Drupal\rds_payment\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access to routes based on login status of current user.
 */
class UnpaidCheck implements AccessInterface {

  /**
   * Grant access (to the payment form) if the user has NOT made a donation this calendar year
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, Route $route) {
    if ($account->id() == 1) {
      $result = AccessResult::allowed();
    }
    elseif (date('z') > 349) {
      $result = AccessResult::forbidden('No payments after Dec 15th')->cachePerPermissions();
    }
    elseif (routedessel_has_paid_this_year($account->id(), TRUE)) {
      $result = AccessResult::forbidden('User already paid this year')->cachePerUser();
    }
    else {
      // For non admin members who haven't paid
      // This isn't cached but maybe the menu block is.
      $result = AccessResult::allowed();
    }
    // the menu item seems to show even when this returns forbidden
    return $result;
  }

}

