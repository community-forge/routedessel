<?php

namespace Drupal\rds_payment\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access to routes based on login status of current user.
 */
class NotDecember implements AccessInterface {


  /**
   * Grant access if the user hasn't placed an order for a product in the current calendar year
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, Route $route) {
    // Hide the link between Dec1 and Jan 4th
    $allowed = date('m' <> 12) and date('z') > 5;
    return AccessResult::allowedIf($allowed)
      ->setCacheMaxAge(strtotime('last day of this month 23:59:59'));
  }

}

