<?php

namespace Drupal\rds_payment\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Users who have more than one lodging link to a list of them
 */
class MyPayments extends MenuLinkDefault implements ContainerFactoryPluginInterface {

  # this isn't called
  public function getRouteParameters() {
    $params = [
      'user' => \Drupal::currentUser()->id()
    ];
    return $params;
  }
}
