<?php

namespace Drupal\rds_payment\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Generate a menu link with query params.
 */
class PendingCheques extends MenuLinkDefault implements ContainerFactoryPluginInterface {

  public function getUrlObject($title_attribute = TRUE) {
    $url = parent::getUrlObject($title_attribute);
    $url->setOption('query', ['complete' => 0]);
    return $url;
  }

}
