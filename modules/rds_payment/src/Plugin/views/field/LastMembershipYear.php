<?php

namespace Drupal\rds_payment\Plugin\views\field;

use Drupal\views\Plugin\views\field\Standard;
use Drupal\views\ResultRow;
use Drupal\views\Attribute\ViewsField;

/**
 * Field handler to provide the last year a user was a member.
 *
 * @ingroup views_field_handlers
 */
#[ViewsField('last_membership_year')]
class LastMembershipYear extends Standard {

  public function query() {
    // virtual field
  }

  /**
   * {@inheritDoc}
   */
  public function render(ResultRow $values) {
    $uid = $values->_entity->id();
    $last_membership = \Drupal::database()->select('give_donation', 'd')
      ->fields('d', ['created'])
      ->condition('uid', $uid)
      ->orderBy('created', 'DESC')
      ->range(0,1)
      ->execute()
      ->fetchField();
    if ($last_membership) {
      // add 21 days to allow for december payments.
      return date('Y', $last_membership + 21*24*3600);
    }
    return '';
  }

}
