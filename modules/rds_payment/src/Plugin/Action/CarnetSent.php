<?php

namespace Drupal\rds_payment\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Action\Attribute\Action;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Flag that the carnet de voyages has been sent for each payment.
 */
#[Action(
  id: 'give_donation_carnet_sent',
  label: new TranslatableMarkup('Carnet envoyée'),
  type: 'RdS'
)]
class CarnetSent extends ActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $entity->envoye->value = 'sent';
    $entity->save();
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = $object->access('update', $account, TRUE);
    $result->andIf(AccessResult::allowedIf($object->envoye->value == 'notyet'));
    return $return_as_object ? $result : $result->isAllowed();
  }
}

