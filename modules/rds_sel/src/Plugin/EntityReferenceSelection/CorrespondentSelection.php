<?php

namespace Drupal\rds_sel\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Component\Utility\Html;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Entity\Attribute\EntityReferenceSelection;

/**
 * Filter users based on their 'fullname'
 */
#[EntityReferenceSelection(
  id: 'correspondent:user',
  label: new TranslatableMarkup(),
  entity_types: ['user'],
  group: 'fullname',
  weight: 2,
  base_plugin_label: 'User full name'
)]
class CorrespondentSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'filter' => [
        'type' => '_none',
        'role' => NULL,
      ]
    ] + parent::defaultConfiguration();
  }


  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);
    $correspondent_uids = rds_sel_all_correspondent_uids();
    $query->condition('uid', $correspondent_uids, 'IN');

    // Parent hasn't added a match condition because the user entity has no 'label' key.
    if (isset($match)) {
      $query->condition('fullname', $match, $match_operator);
    }
    $query->condition('status', 1);
    return $query;
  }

  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    $query = $this->buildEntityQuery($match, $match_operator);
    if ($limit > 0) {
      $query->range(0, $limit);
    }
    $options = [];
    if ($result = $query->execute()) {
      $entities = $this->entityTypeManager->getStorage('user')->loadMultiple($result);
      foreach ($entities as $uid => $user) {
        $options['user'][$uid] = Html::escape(routedessel_unique_name($user));
      }
    }
    return $options;
  }

}
