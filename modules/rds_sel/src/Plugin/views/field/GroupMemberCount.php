<?php
namespace Drupal\rds_sel\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\Attribute\ViewsField;

/**
 * Provides a Views field to display the number of members in a group.
 */
#[ViewsField('rds_group_member_count')]
class GroupMemberCount extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $group_id = $this->getEntity($values)->id();
    $query = "SELECT COUNT(u.uid) "
      . "FROM group_relationship_field_data rfd "
      . "JOIN users_field_data u ON u.uid = rfd.entity_id AND rfd.plugin_id = 'group_membership' "
      . "WHERE rfd.gid = $group_id and u.status = 1";
    return \Drupal::database()->query($query)->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Ensure the field is not empty.
    $this->field_alias = 'group_members_count';
  }

}
