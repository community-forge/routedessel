<?php

namespace Drupal\rds_sel\Plugin\views\field;

use Drupal\views\ResultRow;
use \Drupal\views\Plugin\views\field\Boolean;
use Drupal\views\Attribute\ViewsField;

/**
 * Field handler to provide the last year a user was a member.
 *
 * @ingroup views_field_handlers
 */
#[ViewsField('is_correspondent')]
class IsCorrespondent extends Boolean {

  public function query() {
    // virtual field
  }

  /**
   * {@inheritDoc}
   */
  public function render(ResultRow $values) {
    $uid = $values->_entity->id();

    $query = \Drupal::database()->select('group_relationship_field_data', 'm');
    $query->join('group_content__group_roles', 'r', 'm.id = r.entity_id');
    $query->fields('r', ['entity_id'])
      ->condition('m.entity_id', $uid)
      ->condition('r.group_roles_target_id', 'sel-correspondant');
    $value = !empty($query->execute()->fetchCol());


    if ($this->options['type'] == 'custom') {
      $custom_value = $value ? $this->options['type_custom_true'] : $this->options['type_custom_false'];
      return ViewsRenderPipelineMarkup::create(UtilityXss::filterAdmin($custom_value));
    }
    elseif (isset($this->formats[$this->options['type']])) {
      return $value ? $this->formats[$this->options['type']][0] : $this->formats[$this->options['type']][1];
    }
    else {
      return $value ? $this->formats['yes-no'][0] : $this->formats['yes-no'][1];
    }
  }

}
