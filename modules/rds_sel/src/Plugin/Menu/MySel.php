<?php

namespace Drupal\rds_sel\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\group\GroupMembershipLoader;
use Drupal\user\Entity\User;

class MySel extends MenuLinkDefault implements ContainerFactoryPluginInterface {

  private $currentUser;
  private $groupMembershipLoader;

  /**
   *
   * @param type $configuration
   * @param type $plugin_id
   * @param type $plugin_definition
   * @param type $static_override
   * @param type $current_user
   * @param GroupMembershipLoader $mem_loader
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, $static_override, $current_user, GroupMembershipLoader $mem_loader) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $static_override);
    $this->currentUser = User::load($current_user->id());
    $this->groupMembershipLoader = $mem_loader;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu_link.static.overrides'),
      $container->get('current_user'),
      $container->get('group.membership_loader')// deprecated new name will be group_permission.builder
    );
  }

  public function getRouteParameters() {
    // We need to provide a group id even if the user isn't in a groups (ie user 1)
    // to be able to render the menu manage page.

    $memberships = $this->groupMembershipLoader->loadbyUser($this->currentUser);
    return [
      'group' => $memberships ? reset($memberships)->getGroup()->id() : 20
    ];
  }
}

