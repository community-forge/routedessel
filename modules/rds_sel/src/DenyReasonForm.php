<?php

namespace Drupal\rds_sel;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Mail\MailManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The correspondent gives a reason for denying a member is in their SEL.
 * @todo inject Mailmanager
 */
class DenyReasonForm extends FormBase {

  private $mailManager;

  public function __construct(MailManagerInterface $mail_manager) {
    $this->mailManager = $mail_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.mail')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $group_content = $form_state->getBuildInfo()['args'][0];
    $name = $group_content->getEntity()->get('fullname')->value;
    $form['#title'] = 'Rejet...';
    $form['reason'] = [
      '#type' => 'textarea',
      '#title' => "Merci de nous préciser le motif de votre refus $name ?",
      '#description' => "Ce message sera partagé avec les bénévoles mais pas avec $name."
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Refuser',
      '#weight' => 2
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   * // Grant the adherent role to users and trigger the mail
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $group_content = $form_state->getBuildInfo()['args'][0];

    $new_user = $group_content->getEntity();
    $new_user->save();
    $this->mailManager->mail(
      'rds_sel',
      'user_denied',
      $new_user->getEmail(),
      $new_user->getPreferredLangcode(),
      ['user' => $new_user]
    );
    // Tell the volunteers that a user was refused.
    $this->mailManager->mail(
      'rds_sel',
      'bene_denied',
      \Drupal::config('rds_sel.settings')->get('fallback_correspondent'),
      'fr',
      [
        'user' => $new_user,
        'reason' => $form_state->getValue('reason')
      ]
    );
    $group_content->delete();
    $form_state->setRedirect('entity.user.canonical', ['user' => $user->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'deny_reason_form';
  }

}
