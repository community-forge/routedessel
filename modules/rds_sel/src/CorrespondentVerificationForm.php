<?php

namespace Drupal\rds_sel;

use Drupal\Core\Render\Markup;
use Drupal\mcapi\Entity\Wallet;
use Drupal\mcapi\Entity\Storage\WalletStorage;
use Drupal\group\Entity\Group;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Renderer;
use Drupal\group\GroupMembershipLoader;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides controllers for when group correspondents click links in their
 * emails to approve and disapprove members
 */
class CorrespondentVerificationForm extends FormBase {
  use LoggerChannelTrait;

  private $renderer;
  private $groupMembershipLoader;
  private $sel;

  public function __construct(Renderer $renderer, GroupMembershipLoader $membership_loader) {
    $this->groupMembershipLoader = $membership_loader;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer'),
      $container->get('group.membership_loader'),
    );
  }

  public function getFormId() {
    return 'correspondent_verification_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state, Group $group = NULL) {
    $form_state->set('sel_id', $group->id());
    // Get all members of the group
    $memships = \Drupal::service('group.membership_loader')
      ->loadByGroup($group);
    $verified = [];
    $form['#title'] = 'Verification pour '.$group->label();

    $form['instructions'] = [
      '#markup' => "Veuillez vérifier les membres à jour de leur adhésion dans ".$group->label()." et cocher seulement les membres autorisés à adhérer à la RdSEL<br />
        La croix rouge signifie que ce membre ne propose pas d’hébergement.</br />
        Ce formulaire est  accessible toute l’année.",
      '#weight' => 1
    ];
    $form['members'] = [
      '#type' => 'tableselect',
      '#header' => ['', 'Nom', 'Courriel', 'Téléphone', 'Adhérent en', 'Solde' , 'Hebergement'],
      '#options' => [],
      '#empty' => t('This sel has no members!'),
      '#default_value' => [],
      '#js_select' => FALSE,
      '#weight' => 2
    ];
    // Build the table.
    foreach ($memships as $memship) {
      $user = $memship->getUser();
      $uid = $user->id();
      $wids = \Drupal::entityTypeManager()->getStorage('mc_wallet')->walletsOf($uid);
      if ($wids) {
        $first_wallet = reset($wids);
        $balance = Wallet::load($first_wallet)->balance;
      }
      $phones = $user->phones->view(['label' => 'hidden']);
      $pic = $user->user_picture->view(['label' => 'hidden', 'settings' => ['image_style' => 'thumbnail']]);
      $form['members']['#options'][$uid] = [
        \Drupal::service('renderer')->render($pic),
        $user->toLink(NULL, 'canonical', ['attributes' => ['target' => '_blank']]),
        $user->getEmail(),
        \Drupal::service('renderer')->render($phones),
        $this->lastPaidYear($user),
        \Drupal\mcapi\Functions::formatCurrency($balance),
        is_landlord($uid) ? Markup::create('&#x2713;') : Markup::create('&#x274C'),
        '#disabled' => $user->isActive()
      ];
      $form['members']['#default_value'][$uid] = $user->isActive();
    }
    $form['info'] = [
      '#markup' => "<p>Les membres cochés sont 'vérifiés' et reçoivent un message de validation leur permettant de se connecter et d’adhérer à la RdS.</p></p>",
      '#weight' => '9'
    ];
    $verified_sel_ids = $this->config('rds_newyear.settings')->get('verified_sels');

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Verifier',
      '#weight' => 10
    ];
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $verified_uids = array_filter($form_state->getValue('members'));
    // Activate all accounts.
    foreach ($verified_uids as $uid) {
      // This normally triggers the 'user_status_activated' mail.
      // This message is suppresed in rds_sel_mail_alter() if the user wasn't active last year.
      // See rds_newyear_user_presave() for sending mails to members in and out of the EU
      User::load($uid)->activate()->save();
    }
    $config = \Drupal::configFactory()->getEditable('rds_newyear.settings');
    $verified_sels = $config->get('verified_sels');
    $verified_sels[] = $form_state->get('sel_id');
    $config->set('verified_sels', $verified_sels)->save();
  }


  /**
   * Get the year that a user last paid.
   * @param UserInterface $user
   * @return int
   */
  private function lastPaidYear(UserInterface $user) : int {
    $don_ids = \Drupal::entityQuery('give_donation')->accessCheck(TRUE)
      ->condition('uid', $user->id())
      ->condition('complete', 1)
      ->sort('created', 'DESC')
      ->range(0, 1)
      ->execute();
    if ($don_ids) {
      return date('Y', \Drupal\give\Entity\Donation::load(reset($don_ids))->getCreatedTime());
    }
    return 0;
  }


}
