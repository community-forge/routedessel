<?php

namespace Drupal\rds_sel\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;
use Drupal\group\Entity\Group;

/**
 * Determines access to routes based on whether the user is a correspondent of the SEL
 */
class IsCorrespondent implements AccessInterface {

  /**
   * Grant access if the user is a correspondent of the group, or benevole.
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, Route $route, Group $group) {
    if ($account->hasPermission('administer users')) {
      $result = AccessResult::allowed();
    }
    else {
      $accounts = rds_sel_correspondents($group, TRUE);
      $result = AccessResult::forbidden();
      foreach ($accounts as $acc) {
        if ($acc->id() == $account->id()) {
          $result = AccessResult::allowed();
          break;
        }
      }
    }
    return $result->cachePerUser();
  }

}
