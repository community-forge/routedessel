<?php

namespace Drupal\rds_sel\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access to routes based
 */
class OneSelCheck implements AccessInterface {


  /**
   * Grant access if the user is a member of one and only one SEL
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, Route $route) {
    $sels = get_sels_of_user($account);
    return AccessResult::allowedIf(count($sels) == 1)->cachePerUser();
  }

}

