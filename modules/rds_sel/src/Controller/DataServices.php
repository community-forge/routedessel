<?php

namespace Drupal\rds_sel\Controller;

use Drupal\group\Entity\Group;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
/**
 *
 */
class DataServices extends ControllerBase{

  private $fileSystem;
  private $sitePath;
  private $modPath;

  const CSVFILE = 'public://geo.csv';
  const JSONFILE = 'public://geo.json';

  public function __construct($file_system, $site_path) {
    $this->fileSystem =  $file_system;
    $this->sitePath =  $site_path;
    $this->modPath = \Drupal::service('extension.path.resolver')->getPath('module', 'rds_sel');
  }

  /**
   * @param ContainerInterface $container
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      // Deprecatd in D9 as site path changes from being a service to parameter
      // Change record doesn't say to handle injection https://www.drupal.org/node/30806126
      $container->get('site.path'),
    );
  }

  /**
   * Menu callback
   * Show the site in geojson format.
  */
  function getJson(Request $request) {
    // Output to browser
    if (!file_exists(SELF::JSONFILE)) {
      $this->generateGeoJson();
    }
    return new JsonResponse(file_get_contents(SELF::JSONFILE));
  }

  function getCsv() {
    // Output to browser
    if (!file_exists(SELF::CSVFILE)) {
      $this->generateGeoCsv();
    }
    $response = new Response(file_get_contents(SELF::CSVFILE), 200);
    //$response->headers->set('Content-Disposition', 'attachment; filename="geo.csv"');
    $response->headers->set('Content-type', 'text/csv');
    return $response;
  }

  /**
  * replace the json file.
  */
  private function generateGeoJson() {
    foreach ($this->getAllSel() as $gid) {
      $group = Group::load($gid);
      $geopoints[] = [
        'type' => 'Feature',
        'geometry' => [
          'type' => 'Point',
          'coordinates' => [$group->coordinates->lon, $group->coordinates->lat]
        ],
        'properties' => [
          'title' => $group->toLink(),
          'description' => $group->body->value . '<br />'.$group->mail->value,
        ]
      ];
    }
    $this->fileSystem->saveData(
      json_encode(['type' => 'FeatureCollection', 'features' => $geopoints]),
      SELF::JSONFILE,
      FileSystemInterface::EXISTS_REPLACE
    );
  }


  /**
  * replace the csv file.
  */
  private function generateGeoCsv() {
    $cols = ['title', 'latitude', 'longitude', 'email', 'description', 'url'];
    $rows = [];
    $fp = fopen('php://temp', 'r+');

    fputcsv($fp, $cols);
    foreach ($this->getAllSel() as $gid) {
      $group = Group::load($gid);
      $fields = [
        'title' => $group->label(),
        'latitude' => $group->coordinates->lat,
        'longitude' => $group->coordinates->lon,
        'email' => $group->courriel->value,
        'description' => str_replace(["\r", "\n"], '', $group->body->value),
        'url' => $group->url->value,
      ];
      fputcsv($fp, $fields);
    }
    rewind($fp);
    $this->fileSystem->saveData(
      fread($fp, 999999),
      SELF::CSVFILE,
      FileSystemInterface::EXISTS_REPLACE
    );
    fclose($fp);
  }

  /**
   * Get the ids of all the groups of type 'sel'
   * @return int[]
   *   The group ids
   */
  private function getAllSel() : array {
    return $this->entityTypeManager()
      ->getStorage('group')
      ->getQuery()->accessCheck(TRUE)
      ->condition('type', 'sel')
      ->execute();
  }

  public static function deleteFiles() {
    $fs = \Drupal::service('file_system');
    $fs->delete(SELF::CSVFILE);
    $fs->delete(SELF::JSONFILE);
  }

}
