<?php
namespace Drupal\rds_sel\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Cache\Cache;

/**
 *
 */
class Pages extends ControllerBase {

  function mySel() {
    $groups = get_sels_of_user($this->currentUser());
    return $this->redirect('entity.group.canonical', ['group' => key($groups)]);
  }

  /**
   * A table showing all the members who aren't in a SEL group.
   */
  function orphans() : array {
    return static::orphanTable();
  }

  /**
   * This is abstracted out because it also generates an email to the orphan manager
   * @return array
   *   A renderable array
   */
  static function orphanTable() : array {
    $orphan_uids = static::getOrphanUids();
    if (empty($orphan_uids)) {
      return ['#markup' => 'Tous les membres sont en SELs'];
    }
    // make a table with links to create a SEL for each member
    $renderable = [
      '#title' => 'Membres sans SEL',
      '#type' => 'table',
      '#caption' => count($orphan_uids) . ' utilisateurs',
      '#header' => ['Cree', 'Nom', 'Sel info', ''],
      '#rows' => []
    ];
    $user_data = \Drupal::service('user.data');
    $date_formatter = \Drupal::service('date.formatter');
    /* \Drupal\user\UserInterface $orphan */
    foreach (User::loadMultiple($orphan_uids) as $orphan) {
      /** @var \Drupal\user\Entity\User $orphan */
      $renderable['#rows'][] = [
        'date' => $date_formatter->format($orphan->getCreatedTime(), 'medium'),
        'name' => $orphan->toLink(NULL, 'canonical', ['attributes' => ['target' => '_blank']]),
        'comment' => $user_data->get('rds_sel', $orphan->id(), 'orphan'),
        'link' => $orphan->toLink('Modifier', 'edit-form')
      ];
    }
    return $renderable;
  }

  /**
   * Orphans are blocked accounts which are not in a group.
   *
   * @return int[]
   *
   * @note Most accounts are put into groups the moment they join, but are kicked
   * out if the correspondent rejects them
   *
   * @note We should arrange it so that there no orphaned accounts prior to the
   * current year
   */
  static function getOrphanUids():array {
    $all_blocked_uids = \Drupal::entityQuery('user')->accessCheck(FALSE)
      ->sort('created', 'DESC')
      ->condition('uid', 0, '>')
//      ->condition('created', strtotime('Jan 1st'), '>')
      ->execute();

    $all_member_uids = \Drupal::database()
      ->select('group_relationship_field_data', 'gc')
      ->fields('gc', ['entity_id'])
      ->condition('type', 'sel-group_membership')
      ->distinct()
      ->execute()->fetchCol();
    return array_values(array_diff($all_blocked_uids, $all_member_uids));
  }

  function Promote($group_content) {
    $group_content = \Drupal\group\Entity\GroupRelationship::load($group_content);
    $group_content->set('group_roles', 'sel-correspondant');
    $user = $group_content->getEntity();
    $group_content->save();
    \Drupal::entityTypeManager()->getStorage('group_role')
      ->resetUserGroupRoleCache($group_content->getEntity(), $group_content->getGroup());
    Cache::invalidateTags(['user:'.$user->id()]);
    $url = Url::fromRoute('entity.user.canonical', ['user' => $user->id()]);
    return new RedirectResponse($url->toString());
  }

  function Demote($group_content) {
    $group_content = \Drupal\group\Entity\GroupRelationship::load($group_content);
    $group_content->set('group_roles', []);
    $group_content->save();
    $user = $group_content->getEntity();
    \Drupal::entityTypeManager()->getStorage('group_role')
      ->resetUserGroupRoleCache($user, $group_content->getGroup());
    Cache::invalidateTags(['user:'.$user->id()]);
    $url = Url::fromRoute('entity.user.canonical', ['user' => $user->id()]);
    return new RedirectResponse($url->toString());
  }

}
