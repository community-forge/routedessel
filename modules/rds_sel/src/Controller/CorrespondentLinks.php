<?php

namespace Drupal\rds_sel\Controller;

use Drupal\group\Entity\GroupRelationship;
use Drupal\group\Entity\GroupInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\Crypt;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Logger\LoggerChannelTrait;

/**
 * Provides controllers for when group correspondents click links in their
 * emails to approve and disapprove members
 */
class CorrespondentLinks implements ContainerInjectionInterface {

  use LoggerChannelTrait;

  const ACTION_APPROVE = 'verify';
  const ACTION_DENY = 'reject';

  /**
   * @var FormBuilderInterface
   */
  private $formBuilder;

  /**
   * @param FormBuilderInterface $form_builder
   */
  function __construct(FormBuilderInterface $form_builder) {
    $this->formBuilder = $form_builder;
  }

  /**
   * @param ContainerInterface $container
   * @return \static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder'),
    );
  }

  /**
   * The correspondent clicks an email link to confirm a member
   *
   * @param int $group_content_id
   * @param string $hash
   * @param string $action
   * @return Response
   */
  function verifyMember(int $group_content_id, string $hash, string $action) {
    if ($group_content = GroupRelationship::load($group_content_id)) {
      if ($hash = self::correpondentRehash($group_content_id, $action)) {
        $user = $group_content->getEntity();
        if ($action == self::ACTION_APPROVE) {
          $user->activate()->save(); // @see rds_payment_user_presave()
          return [
            '#markup' => Markup::create('Merci pour votre assistance ! <a href="" onClick="window.close();">Fermer la fenêtre</a>.')
          ];
        }
        // Orphan the user. This means that the approve link will be invalid.
        else {//if ($action == self::ACTION_DENY) {
          // this should have no effect because the is already blocked and without roles.
          $user->removeRole(RID_ADHERENT);
          $user->block()->save();
          $this->getLogger('sel')->notice('Member '.$user->id(). ' NOT verified');
          // Present the correspondent with a form giving the reason.
          return $this->formBuilder->getForm('\Drupal\rds_sel\DenyReasonForm', $group_content);
        }
      }
      else {
        $this->getLogger('sel')->error('The correspondant clicked to confirm whether '.$group_content->getEntity()->id() .' is a real member of group '. $group_content->getGroup()->id().', but the hash was invalid');
      }
    }

    return ['#markup' => 'bad link'];
  }

  /**
   * Generate links for the correspondent to approve or disapprove new members.
   * @param GroupRelationship $group_membership
   * @param string $action
   * @return string
   *   The Url
   */
  static function selGroupCorrespondantApprovalUrl(GroupRelationship $group_membership, string $action) : string {
    $membership_id = $group_membership->id();
    return \Drupal::urlGenerator()->generateFromRoute(
      'rds_sel.correspondent.action',
      [
        'group_content_id' => $membership_id,
        'hash' => self::correpondentRehash($membership_id, $action),
        'action' => $action
      ],
      ['absolute' => TRUE, 'https' => TRUE]
    );
  }

  /**
   * Generate a hash for group correspondent email actions.
   * @param GroupInterface $sel
   * @param UserInterface $new_user
   * @param string $action
   * @return string
   *   The hash
   */
  private static function correpondentRehash($membership_id, string $action) {
    return Crypt::hmacBase64($membership_id . $action, Settings::getHashSalt());
  }

}
