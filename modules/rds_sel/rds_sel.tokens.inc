<?php

use Drupal\rds_sel\Controller\CorrespondentLinks;
use Drupal\group\Entity\GroupRelationship;


/**
 * Implements hook_tokens().
$user = \Drupal\user\Entity\User::load(19400);
_user_mail_notify('register_pending_approval', $user);
 */
function rds_sel_tokens($type, $tokens, array $data, array $options, $bubbleable_metadata) {
  $replacements = [];

  // replace the [sel:*] tokens
  if ($type == 'sel' || $type == 'correspondent' and isset($data['user'])) {
    $sels = get_sels_of_user($data['user'], TRUE);
    $sel = reset($sels);
    if ($type == 'sel') {
      $replacements += rds_sel_pseudo_replace(
        'sel',
        'group',
        $tokens,
        ['group' => $sel],
        $options,
        $bubbleable_metadata
      );
    }
    elseif($type == 'correspondent' and $sel and $correspondents = rds_sel_correspondents($sel)) {
      // if there is no correspondent the token is not replaced. What to do?
      $replacements += rds_sel_pseudo_replace(
        'correspondent',
        'user',
        $tokens,
        ['user' => reset($correspondents)],
        $options,
        $bubbleable_metadata
      );
    }
  }

  if ($type == 'user' and isset($data['user'])) {
    $memberships = GroupRelationship::loadByEntity($data['user']); // assumes only be one relationship!
    $membership = reset($memberships);
    if (isset($tokens[CorrespondentLinks::ACTION_APPROVE])) {
      $replacements[$tokens[CorrespondentLinks::ACTION_APPROVE]] = CorrespondentLinks::selGroupCorrespondantApprovalUrl($membership, CorrespondentLinks::ACTION_APPROVE);
    }
    if (isset($tokens[CorrespondentLinks::ACTION_DENY])) {
      $replacements[$tokens[CorrespondentLinks::ACTION_DENY]] = CorrespondentLinks::selGroupCorrespondantApprovalUrl($membership, CorrespondentLinks::ACTION_DENY);
    }
  }
  return $replacements;
}

/**
 *
 * @param string $pseudo
 *   token type
 * @param string $type
 *   entity type
 * @param array $tokens
 * @param array $data
 * @param array $options
 * @param type $bubbleable_metadata
 * @return array
 */
function rds_sel_pseudo_replace(string $pseudo, string $type, array $tokens, array $data, array $options, $bubbleable_metadata) : array {
  $replacements = [];
  array_walk(
    $tokens,
    function (&$tok) use ($type, $pseudo) {
      $tok = str_replace("$pseudo:", "$type:", $tok);
    }
  );
  $results = \Drupal::token()->generate(
    $type,
    $tokens,
    $data,
    $options,
    $bubbleable_metadata
  );
  foreach ($results as $key => $val) {
    $new_key = str_replace("$type:", "$pseudo:", $key);
    $replacements[$new_key] = $val;
  }
  return $replacements;
}

/**
 * Implements hook_token_info().
 */
function rds_sel_token_info() {
  return [
    'types' => [
      'sel' => [
        'name' => 'SEL',
        'description' => 'Tokens related to individual nodes.',
        'needs-data' => 'group',
      ],
      'correspondent' => [
        'name' => 'Correspondent',
        'description' => 'Tokens related the (first) correspondent of the SEL of the user',
        'needs-data' => 'user'
      ],
    ],
    'tokens' => [
      'sel' => group_token_info()['tokens']['group'],
      'correspondent' => user_token_info()['tokens']['user'],
      'user' => [
        CorrespondentLinks::ACTION_APPROVE => [
          'name' => 'Approving new member link'
        ],
        CorrespondentLinks::ACTION_DENY => [
          'name' => 'Denying new member link'
        ]
      ]
    ]
  ];
}
