<?php

use Drupal\rds_sel\Controller\Pages;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRelationship;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

const GROUP_RID_CORRESPONDENT = 'sel-correspondant';

/**
 * Implements hook_entity_extra_field_info().
 */
function rds_sel_entity_extra_field_info() {
  return [
    'user' => [
      'user' => [
        'form' => [
          'applytosel' => [
            'label' => 'Postulez à SEL',
            'description' => 'The SEL will be notified',
            'weight' => 10
          ]
        ],
        'display' => [
          'memberofsel' => [
            'label' => 'Member of SEL',
            'description' => 'the name of the SEL which this member is in',
            'weight' => 1
          ]
        ]
      ]
    ],
    'group' => [
      'sel' => [
        'display' => [
          'correspondents' => [
            'label' => 'Correspondents',
            'description' => 'Links to correspondents',
            'weight' => 4
          ]
        ]
      ]
    ]
  ];
}

/**
 * Implements hook_group_view().
 */
function rds_sel_group_view(array &$build, $group, $display, $view_mode) {
  $contact_form = FALSE;
  if (\Drupal::currentUser()->isAuthenticated()) {
    foreach (rds_sel_correspondents($group) as $account) {
      if (\Drupal::currentUser()->id() == $account->id()) {
        $contact_form = TRUE;
      }
      $links[] = $account->toLink()->toString();
    }
    if (empty($links)) {
      $links[] = "Pas des correspondants";
    }
    $build['correspondents'] = [
      '#markup' => implode('<br />', $links),
      '#access' => \Drupal::currentUser()->hasPermission('access user contact forms')
    ];
  }
  $build['correspondent_edit_form'] = [
    '#title' => 'Change these details...',
    '#type' => 'link',
    '#url' => Url::fromRoute(
      'entity.contact_form.canonical',
      ['contact_form' =>  'correspondent']
    ),
    '#weight' => -10,
    '#access' => $contact_form
  ];
}

/**
 * Implements hook_form_FORM_ID_alter().
 * Change the field name of the contact form.
 */
function rds_sel_form_contact_message_correspondent_form_alter(&$form, $form_state) {
  $form['subject']['widget'][0]['value']['#title'] = 'Nom du SEL';
  $form['#validate'][] = 'rds_sel_correspondent_message_validate';
}

// Append the SEL link to the end of the message.
function rds_sel_correspondent_message_validate($form, $form_state) {
  /** @var \Drupal\group\Entity\Group $sel */
  $sels = get_sels_of_user(\Drupal::currentUser(), TRUE);
  if (count($sels) == 1) {
    $message = $form_state->getValue('message');
    $message[0]['value'] = reset($sels)->toLink('Edit SEL', 'edit-form', ['absolute' => TRUE, 'https' => TRUE])->toString(). "\n\n".$message[0]['value'];
    $form_state->setValue('message', $message);
  }
}

/**
 * Implements hook_user_view().
 *
 * Add extra_fields to user display.
 */
function rds_sel_user_view(array &$build, $entity, $display, $view_mode) {
  if ($display->getComponent('memberofsel')) {
    // Could have used get_sels_of_user() if we didn't need roles as well.
    foreach (\Drupal::service('group.membership_loader')->loadByUser($entity) as $membership) {
      if ($membership->getGroup()->getGroupType()->id() == 'sel') {
        $sel = $membership->getGroup();
        $sels[$sel->id()] = $sel->toLink()->toString();
        $group_roles = $membership->getRoles();
        $role_info = '';
        if (isset($group_roles['sel-correspondant'])) {
          $role_info = $group_roles['sel-correspondant']->label();
        }
        if (\Drupal::currentUser()->hasPermission('administer users')) {
          if (isset($group_roles['sel-correspondant'])) {
            $role_info .= Link::fromTextAndUrl(
              ' Demote',
              Url::fromRoute('rds_sel.group_relationship.corr_demote', ['group_content' => $membership->getGroupRelationship()->id()])
            )->toString();
          }
          else {
            $role_info .= Link::fromTextAndUrl(
              'Promote to correspondent',
              Url::fromRoute('rds_sel.group_relationship.corr_promote', ['group_content' => $membership->getGroupRelationship()->id()])
            )->toString();
          }
        }
        $sels[$sel->id()] .= ' ('.$role_info.')';
      }
    }
    if (!isset($sels)) {
      $sels = ['SANS SEL'];
    }
    // Simulate how normal fields are viewed
    $text = '<div id = "member-of-sels">'.implode('<br />', $sels).'</div>';
    $build['memberofsel'] = [
      '#markup' => Markup::create($text)
    ];
  }
  return $build;
}

/**
 * Utility
 *
 * @param AccountInterface $account
 * @return Group[]
 */
function get_sels_of_user(AccountInterface $account, bool $full = FALSE) : array {
  if (!$account instanceof Drupal\user\Entity\User) {
    $account = \Drupal\user\Entity\User::load($account->id());
  }
  $sels = [];
  if (!$account->isNew()) {
    foreach (GroupRelationship::loadByEntity($account) as $membership) {
      if ($membership->getRelationshipType()->getGroupType()->id() == 'sel') {
        $sel = $membership->getGroup();
        $sels[$sel->id()] = $full ? $sel : $sel->label();
      }
    }
  }
  return $sels;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function rds_sel_form_group_type_edit_form_alter(&$form, $form_state)  {
  $settings = \Drupal::Config('rds_sel.settings');

  $uids = \Drupal::entityQuery('user')->accessCheck(TRUE)
    ->condition('roles', 'benevole')
    ->execute();
  foreach (User::loadMultiple($uids) as $account) {
    $options[$account->id()] = $account->getDisplayName();
  }
  $form['fallback_correspondent'] = [
    '#title' => 'Correspondant de repli',
    '#description' => "Vérifie les membres lorsqu'un sel n'a pas de correspondant.",
    '#type' => 'email',
    '#required' => TRUE,
    '#weight' => 8,
    '#default_value' => $settings->get('fallback_correspondent')
  ];
  $form['sel_manager'] = [
    '#title' => 'Gestionnaire des SEL',
    '#description' => "Veille à ce qu'il y ait un SEL pour chaque utilisateur.",
    '#type' => 'email',
    '#required' => TRUE,
    '#weight' => 9,
    '#default_value' => $settings->get('sel_manager')
  ];
  $form['correspondent_manager'] = [
    '#title' => 'Gestionnaire des correspondents',
    '#description' => "Veille à ce qu'il y ait un correspondant pour chaque SEL.",
    '#type' => 'email',
    '#required' => TRUE,
    '#weight' => 10,
    '#default_value' => $settings->get('correspondent_manager')
  ];
  $form['orphan_manager'] = [
    '#title' => 'Gestionnaire des utilisateurs orphelins',
    '#description' => "Traite les utilisateurs rejetés en tant que membres du SEL.",
    '#type' => 'email',
    '#required' => TRUE,
    '#weight' => 10,
    '#default_value' => $settings->get('orphan_manager')
  ];

  //mail templates
  $form['register_new_sel'] = [
    '#type' => 'details',
    '#title' => t('Notify SEL manager of new SEL'),
    '#open' => TRUE,
    '#description' => t('Must research the SEL and create it and add the new member.'),
    '#group' => 'email',
    'register_new_sel_subject' => [
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#default_value' => $settings->get('register_new_sel.subject'),
      '#maxlength' => 180,
      '#required' => 1
    ],
    'register_new_sel_body' => [
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#description' => t('Global tokens available plus [user:*] and [sel:*]'),
      '#default_value' => $settings->get('register_new_sel.body'),
      '#rows' => 15,
      '#required' => 1
    ]
  ];
  $form['user_denied'] = [
    '#type' => 'details',
    '#title' => "Notifier un utilisateur non validé",
    '#open' => TRUE,
    '#description' => "Le nouveau membre est informé que le correspondant du SEL qu'il a choisi ne l'a pas reconnu.",
    '#group' => 'email',
    'user_denied_subject' => [
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#default_value' => $settings->get('user_denied.subject'),
      '#maxlength' => 180,
      '#required' => 1
    ],
    'user_denied_body' => [
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#description' => t('Global tokens available plus [sel:*]'),
      '#default_value' => $settings->get('user_denied.body'),
      '#rows' => 15,
      '#required' => 1
    ]
  ];
  $form['bene_denied'] = [
    '#type' => 'details',
    '#title' => "Notifier à benevole un utilisateur non validé.",
    '#open' => TRUE,
    '#description' => "Benevoles est informé que le correspondant du SEL a refusé un membre.",
    '#group' => 'email',
    'bene_denied_subject' => [
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#default_value' => $settings->get('bene_denied.subject'),
      '#maxlength' => 180,
      '#required' => 1
    ],
    'bene_denied_body' => [
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#description' => "The correspondent's reason will be appended to this template. Global tokens available plus [correspondent:*], and [user:*], [sel:*]. ",
      '#default_value' => $settings->get('bene_denied.body'),
      '#rows' => 15,
      '#required' => 1
    ]
  ];
  $form['actions']['submit']['#submit'][] = 'rds_sel_email_settings_submit';
}

function rds_sel_email_settings_submit($form, $form_state) {
  \Drupal::configFactory()->getEditable('rds_sel.settings')
    ->set('fallback_correspondent', $form_state->getValue('fallback_correspondent'))
    ->set('sel_manager', $form_state->getValue('sel_manager'))
    ->set('correspondent_manager', $form_state->getValue('correspondent_manager'))
    ->set('orphan_manager', $form_state->getValue('orphan_manager'))
    ->set('register_new_sel.subject', $form_state->getValue('register_new_sel_subject'))
    ->set('register_new_sel.body', $form_state->getValue('register_new_sel_body'))
    ->set('user_denied.subject', $form_state->getValue('user_denied_subject'))
    ->set('user_denied.body', $form_state->getValue('user_denied_body'))
    ->set('bene_denied.subject', $form_state->getValue('bene_denied_subject'))
    ->set('bene_denied.body', $form_state->getValue('bene_denied_body'))
    ->save();
}

/**
 * Implements hook_form_user_form_alter().
 *
 * New members or non volunteers MUST add a SEL if they don't have one.
 * Benevoles can add and remove SELs
 */
function rds_sel_form_user_form_alter(&$form, $form_state) {
  if ($component = $form_state->get('form_display')->getComponent('applytosel')) {
    $account = $form_state->getFormObject()->getEntity();
    $current_user = \Drupal::currentUser();
    $mult = $current_user->hasPermission('administer users') or !$account->isNew() && is_correspondent($account);

    $q = \Drupal::database()->select('groups_field_data', 'g');
    $q->join('group__address', 'a', 'g.id = a.entity_id');
    $sels = $q->fields('g', ['id', 'label'])
      ->fields('a', ['address_administrative_area', 'address_country_code'])
      ->orderBy('g.label')
      ->execute()->fetchAll();

    $countries = \Drupal::service('country_manager')->getStandardList();
    foreach ($sels as $sel) {
      $name = $sel->label;
      if ($area = $sel->address_administrative_area) {
        if (!preg_match('/[A-Z][0-9]+/', $area)) {
          $name .= " ($area)";
        }
      }
      $country_name = (string)$countries[$sel->address_country_code];
      $list_of_SELs[$country_name][$sel->id] = $name;
    }
    $user_sels = get_sels_of_user($account, FALSE);
    $form['applytosel'] = [
      '#title' => 'Mon SEL',
      '#description' => $mult ?
        'Sélectionnez votre SEL, (votre SEL principal. Si vous êtes dans des SEL supplémentaires, vous pouvez les mentionner ci-dessous). Le correspondant de votre SEL devra confirmer que vous êtes bien membre de ce SEL.' :
        'Sélectionnez votre SEL',
      '#type' => 'select',
      '#options' => $list_of_SELs,
      '#required' => $account->id() != 1 and empty($user_sels) and !$account->isNew(),
      '#default_value' => array_keys($user_sels),
      '#empty_option' => ">> Mon SEL n'est pas dans le liste (voir ci-dessous)", // when #multiple = false
      '#multiple' => $mult,
      '#size' => $mult ? 18 : 1,
      // this field is only enabled if you are admin or if you are not in a SEL
      '#weight' => $component['weight'],
      '#field_validation' => ['rds_sel_applyto_field_validation']
    ];
    $form['new_sel_info'] = [
      '#type' => 'textfield',
      '#title' => "L'url de votre SEL, ou d'autres détails de contact.",
      '#description' => "Nous contacterons votre SEL et l'enregistrerons dans notre base de données.",
      '#states' => [
        'visible' => [
         ':input[name="applytosel"]' => ['value' => ''],
        ]
      ],
      '#weight' => $component['weight'] + 0.5
    ];
    // Replace the form ::save submit callback with my own.
    $pos = array_search('::save', $form['actions']['submit']['#submit']);
    $form['actions']['submit']['#submit'][$pos] = 'rds_sel_user_registered_save';
  }
}

/**
 * Field validation callback
 * Ensure the user doesn't leave a SEL without joining another.
 */
function rds_sel_applyto_field_validation(&$element, $form_state) {
  $wouldbein = count((array)$form_state->getValue('applytosel')) + !empty($form_state->getValue('new_sel_info'));
  if ($wouldbein < 1) {
    $form_state->setError($element, "Vous devez être membre d'un SEL");
  }
  list($leaving, $joining) = rds_sel_diff_group($account, (array)$form_state->getValue('applytosel'));
  if (count($joining) > 1) {
    $form_state->setError($element, "Vous ne pouvez adhérer qu'à une seule SEL à la fois");
  }
}

/**
 * Form submission handler
 * Replaces UserForm::save
 *
 * Put every newly registered member in a group.
 */
function rds_sel_user_registered_save($form, $form_state) {
  // The form->entity is protected but I can build it again.
  $account = $form_state->getFormObject()->getEntity();
  $was_new = $account->isNew();
  $account->save();
  $form_state->set('user', $account);
  $form_state->setValue('uid', $account->id());

  // The (new) user must be put in the group after being saved but before triggering the email.
  list($leaving, $joining) = rds_sel_diff_group($account, (array)$form_state->getValue('applytosel'));
  foreach ($leaving as $gid) {
    $sel = Group::load($gid);
    if ($mem = \Drupal::service('group.membership_loader')->load($sel, $account)) {
      $mem->getGroupRelationship()->delete();
      \Drupal::messenger()->addStatus('Vous avez quitté '.$mem->getGroup()->label());
    }
  }
  foreach (array_filter($joining) as $gid) {
    // The user having been saved, it can now be added to the group.
    \Drupal::service('account_switcher')->switchTo(User::load(1));
    Group::load($gid)->addMember($account);
    \Drupal::service('account_switcher')->switchBack();
  }
  if (empty($joining) and $info = $form_state->getValue('new_sel_info')) {
    \Drupal::service('user.data')->set('rds_sel', $uid, 'orphan', $info);
    // Notify the fallback SEL Manager
    \Drupal::service('plugin.manager.mail')->mail(
      'rds_sel',
      'register_new_sel',
      \Drupal::config('rds_sel.settings')->get('sel_manager'),
      'fr',
      ['user' => $account]
    );
    // save the contact info in userdata
    \Drupal::messenger()->addStatus("Merci de votre candidature. L'un de nos bénévoles vous contactera bientôt.");
  }
  if ($was_new) {// Notify correspondent if account is not verified.
    \Drupal::service('account_switcher')->switchTo(User::load(1));// otherwise the username renders anonymously.
    _user_mail_notify('register_pending_approval', $account);
    \Drupal::service('account_switcher')->switchBack();
    \Drupal::messenger()->addStatus("Merci pour votre demande de compte. Votre compte est actuellement en attente de validation par l'administrateur du site.<br />Une fois que vous êtes validé, un message de bienvenue avec des instructions complémentaires a été envoyé à votre adresse de courriel.");
    \Drupal::logger('user')->notice(
      'New user: %name %email.',
      [
        '%name' => $account->getDisplayName(),
        '%email' => '<' . $form_state->getValue('mail') . '>', 'type' => $account->toLink(t('Edit'), 'edit-form')->toString()
      ]
    );
    $form_state->setRedirect('<front>');
  }
  else {
    $form_state->setRedirect('entity.user.canonical', ['user' => $account->id()]);
  }
}

/**
 * determine the difference between the groups the user is in, and the groups given.
 * @return [][]
 */
function rds_sel_diff_group(UserInterface $account, array $new_gids) : array {
  $current_groups = [];
  foreach (\Drupal::service('group.membership_loader')->loadByUser($account) as $membership) {
    $group = $membership->getGroup();
    $current_groups[$group->id()] = $group;
  }
  return[
    array_diff(array_keys($current_groups), $new_gids),
    array_diff($new_gids, array_keys($current_groups))
  ];
}

/**
 * Implements hook_mail().
 */
function rds_sel_mail($key, &$message, $params) {
  switch($key) {
    case 'user_denied':
    case 'bene_denied':
    case 'register_new_sel':
      $template = \Drupal::Config('rds_sel.settings')->get($key);
      $message['subject'] = \Drupal::token()->replace($template['subject'], $params);
      $message['body'][] = \Drupal::token()->replace($template['body'], $params);
      if ($key == 'bene_denied') {
        $message['body'][] = '"'.$params['reason'].'"';
      }
      break;
    case 'orphans':
      $renderable = Pages::orphanTable();
      $message['subject'] = 'Route des SEL: Membres Sans SEL';
      $message['body'][] = \Drupal::service('renderer')->render($renderable);
      break;
    case 'selsanscorrs':
      $message['subject'] = 'Liste mensuelle des SELs sans correspondants';
      foreach (Group::loadMultiple($params['gids']) as $sel) {
        $message['body'][] = $sel->toLink()->toString().'<br />';
      }
      break;
  }
}

/**
 * Implements hook_mail_alter().
 */
function rds_sel_mail_alter(&$message) {
  if ($message['id'] == 'user_register_pending_approval_admin') {
    // Change the recipient from admin whoever, to the sel correspondent(s).
    if ($sels = get_sels_of_user($message['params']['account'], TRUE)) {
      $sel = reset($sels);
      if ($correspondents = rds_sel_correspondents($sel)) {
        foreach ($correspondents as $acc) {
          $to[] = $acc->getEmail();
        }
      }
      else {
        $to[] = \Drupal::Config('rds_sel.settings')->get('fallback_correspondent');
      }
      $message['to'] = implode(', ', $to);
    }
    else {
      // Don't send if a member isn't in a SEL. @see rds_sel_user_registered
      $message['send'] = 0;
    }
  }

  if ($message['id'] == 'user_register_pending_approval') {
    if (!get_sels_of_user($message['params']['account'], TRUE)) {
      // Don't send if a member isn't in a SEL. @see rds_sel_user_registered
      $message['send'] = 0;
    }
  }

  // Only send the user activation mail if the user is new this year.
  // See rds_payment_user_presave() for when the user is re-validated at start of year.
  if ($message['id'] == 'user_status_activated') {
    $created = $message['params']['account']->getCreatedTime();
    $created_year = date('Y', $created);
    if ($created_year < date('Y')) {
      $message['send'] = 0;
    }
  }
}

/**
 * Get the correspondents for a group.
 *
 * @param Group $group
 * @return UserInterface[]
 *   May be empty.
 */
function rds_sel_correspondents(Group $group) : array {
  $accounts = [];
  $memships = \Drupal::service('group.membership_loader')
    ->loadByGroup($group, [GROUP_RID_CORRESPONDENT]);
  foreach ($memships as $memship) {
    $accounts[] = $memship->getUser();
  }
  return $accounts;
}

/**
 * @return int[]
 */
function rds_sel_all_correspondent_uids() : array {
  $uids = [];
  $memshipIds = \Drupal::database()
    ->select('group_content__group_roles', 'gr')
    ->fields('gr', ['entity_id'])
    ->condition('group_roles_target_id', 'sel-correspondant')
    ->execute()->fetchCol();
  if ($memshipIds) {
    $uids = \Drupal::database()
      ->select('group_relationship_field_data', 'd')
      ->fields('d', ['entity_id'])
      ->condition('type', 'sel-group_membership')
      ->condition('id', $memshipIds, 'IN')
      ->condition('entity_id', 1, '<>') // actually user 1 shouldn't be in any groups...
      ->distinct()
      ->execute()->fetchCol();
  }
  return array_unique($uids);
}

/**
 * Implements hook_ENTITY_TYPE_update().
 *
 * Delete the cached csv and json for the cforge map.
 */
function rds_sel_group_update($group) {
  \Drupal\rds_sel\Controller\DataServices::deleteFiles();
}


/**
 * Utility
 *
 * @param Drupal\Core\Session\AccountInterface $account
 * @return boolean
 */
function is_correspondent(AccountInterface $account) : bool {
  $memberships = \Drupal::service('group.membership_loader')->loadByUser($account);
  foreach ($memberships as $mem) {
    foreach ($mem->getRoles() as $role) {
      if ($role->id() == GROUP_RID_CORRESPONDENT) {
        return TRUE;
      }
    }
  }
  return FALSE;
}

/**
 * Implements hook_cron().
 */
function rds_sel_cron() {
  $request_time = \Drupal::time()->getRequestTime();
  $last_cron = \Drupal::state()->get('system.cron_last');

  // Orphan manager to receive a weekly mail with the contents of page /orphans
  // Every Monday
  if (date('N', $last_cron) ==  1 and date('N') == 2) {
    \Drupal::service('plugin.manager.mail')->mail(
      'rds_sel',
      'orphans',
      \Drupal::Config('rds_sel.settings')->get('orphan_manager'),
      'fr',
      []
    );
  }

  // The gestionaire des correspondents should receive an email each month showing
  // a list of sels sans correspondants.
  // @todo
  if (date('W', $request_time) > date('W', $last_cron)) {
    if ($no_corr_sels = rds_sels_sans_correspondents()) {
      \Drupal::service('plugin.manager.mail')->mail(
         'rds_sel',
         'selsanscorrs',
         \Drupal::config('rds_sel.settings')->get('correspondent_manager'),
         'fr',
         [
           'gids' => array_values($no_corr_sels)
         ]
      );
    }
  }
}

/**
 * Utility, find all the sels with no correspondent members.
 * @return array
 *   The group ids.
 */
function rds_sels_sans_correspondents() : array {
  // Subtract all the SELs with correspondents from all the SELs
  $all_sels = \Drupal::entityTypeManager()->getStorage('group')
    ->getQuery()->accessCheck(TRUE)
    ->condition('status', 1)
    ->execute();
  // All the SELS with correspondents.
  $corr_memberships = \Drupal::entityQuery('group_content')->accessCheck(TRUE)
    ->condition('group_roles', 'sel-correspondant')
    ->execute(); // This was giving trouble on dev.
  $corr_memberships = \Drupal::database()->select('group_content__group_roles', 'r')
    ->condition('group_roles_target_id', 'sel-correspondant')
    ->fields('r', ['entity_id'])
    ->execute()
    ->fetchCol();
  $sels_with_corrs = \Drupal::database()
    ->select('group_relationship_field_data', 'rel')
    ->condition('id', $corr_memberships, 'IN')
    ->fields('rel', ['gid'])
    ->execute()
    ->fetchCol();
  return array_values(array_diff($all_sels, $sels_with_corrs));
}

/**
 * Implements hook_views_pre_build();
 * Set the country filter to France if it is not set.
 */
function rds_sel_views_pre_build($view) {
  if ($view->id() == 'sels') {
    if (empty($view->getExposedInput())) {
      $view->setExposedinput(['country' => 'FR']);
    }
  }
}

/**
 * Implements hook_form_views_exposed_form_alter().
 */
function rds_sel_form_views_exposed_form_alter(&$form, $form_state) {
  if ($form_state->get('view')->id() == 'sels' and isset($form['country'])) {
    // Todo get the country names.
    // Todo show only countries with active nodes.
    $query = \Drupal::database()->select('group__address', 'a');
    $query->join('groups_field_data', 'g', 'g.id = a.entity_id');
    $countries = $query->fields('a', ['address_country_code'])
      ->condition('g.status', 1)
      ->condition('g.type', 'sel')
      ->distinct()
      ->execute()
      ->fetchCol();
    $form['country']['#options'] = array_intersect_key($form['country']['#options'], array_flip($countries));
  }
}

/**
 * Implements hook_entity_field_access().
 */
function rds_sel_entity_field_access($operation, \Drupal\Core\Field\FieldDefinitionInterface $field_definition, \Drupal\Core\Session\AccountInterface $account, \Drupal\Core\Field\FieldItemListInterface $items = NULL) {
  if ($operation == 'view' and $field_definition->getTargetEntityTypeId() == 'group') {
    if ($field_definition->getName() == 'phone') {
      return AccessResult::forbiddenIf(!$account->hasPermission('access user contact forms'))->cachePerPermissions();
    }
  }
  return AccessResult::neutral();
}

/**
 * Implements hook_field_widget_single_element_WIDGET_TYPE_form_alter().
 */
function rds_sel_field_widget_single_element_dynamic_entity_reference_default_form_alter(&$element, $form_state, $context) {
  if ($element['target_id']['#selection_handler'] == 'default:user') {
    $element['target_id']['#selection_handler'] = 'fullname:user';
  }
}

/**
 * Implements hook_entity_type_alter().
 *
 * @temp
 */
function rds_sel_entity_type_alter(array &$entity_types) {
  $entity_types['group_content']->setLinkTemplate('correspondent-promote', '/group-content/{group_content}/promote');
  $entity_types['group_content']->setLinkTemplate('correspondent-demote', '/group-content/{group_content}/demote');
}

// temp until group 2.0
function __rds_sel_group_access($group, $operation, $account) {
  if ($account->hasPermission('administer users')) {
    return AccessResult::allowed()->cachePerPermissions();
  }
}

/**
 * Implements hook_views_data().
 */
function rds_sel_views_data() {
  $data = [
    'users' => [
      'is_correspondent' => [
        'title' => t('Is Correspondent'),
        'field' => [
          'id' => 'is_correspondent',
          'real field' => FALSE,
          'click sortable' => FALSE,
          'no group by' => TRUE,
          'views field' => TRUE
        ]
      ]
    ]
  ];
  return $data;
}

/**
 * Implementes hook_views_data_alter().
 */
function rds_sel_views_data_alter(&$data) {
  $data['groups_field_data']['group_members_count']['field']['id'] = 'rds_group_member_count';
  $data['groups_field_data']['group_members_count']['field']['title'] = 'Group member count';
}
