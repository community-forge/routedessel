<?php

namespace Drupal\rds_ad;

use Drupal\token\TokenInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form builder for settings.
 */
class ModerateForm extends FormBase {

  /**
   * @var \Drupal\node\Entity\Node
   */
  private $node;
  /**
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * @var Drupal\token\TokenInterface
   */
  private $tokenService;

  /**
   * @var Drupal\Core\Mail\MailManagerInterface
   */
  private $mailManager;

  const MESSAGE = 'Cher [user:display-name],

Avant de partager votre annonce avec le réseau, notre modérateur [current-user:display-name] a demandé une modification.

[REASON]

<a href="[node:edit-url]">Edit votre annonce</a>

Meilleures salutations de la part de l\'équipe';

  function __construct(EntityTypeManagerInterface $entity_type_manager, TokenInterface $token_service, MailManagerInterface $mail_manager) {
    $this->node = $this->getRequest()->get('node');
    $this->entityTypeManager = $entity_type_manager;
    $this->tokenService = $token_service;
    $this->mailManager = $mail_manager;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('token'),
      $container->get('plugin.manager.mail'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rds_annonce_moderate';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($this->node->bundle() <> 'annonce') {
      die('only annonce can be moderated');
    }
    $owner = $this->node->getOwner();

    $form['#title'] = "Moderer l'annonce de ".$owner->getDisplayName();

    $form['preview'] = $this->entityTypeManager->getViewBuilder('node')->view($this->node);
    $form['preview']['#weight'] = -1;
    $form['approve'] = [
      '#type' => 'submit',
      '#value' => 'Approuver et faire la queue',
      '#weight' => 1
    ];

    $prefix = substr(SELF::MESSAGE, 0, strpos(SELF::MESSAGE, '[REASON]'));
    $suffix = substr(SELF::MESSAGE, strpos(SELF::MESSAGE, '[REASON]') + 8);

    $form['feedback'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => 'Rejection email to '.$owner->getEmail(),
      'prefix' => [
        '#markup' => $this->tokenService->replace($prefix, ['user' => $owner, 'node' => $this->node]),
        '#weight' => -1
      ],
      'message' => [
        '#type' => 'textarea',
        '#rows' => 3,
        '#weight' => 0
      ],
      'suffix' => [
        '#markup' => $this->tokenService->replace($suffix, ['user' => $owner, 'node' => $this->node]),
        '#weight' => 1
      ],
      '#weight' => 2
    ];

    $form['reject'] = [
      '#type' => 'submit',
      '#value' => 'Rejeter',
      '#weight' => 3
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    if ($trigger['#value'] == 'Approuver et faire la queue') {
      // Publish the node and put it in the send queue using the promote bit
      $this->node->setPromoted(TRUE)// promoted means it is waiting to be broadcast.
        ->setPublished()
        ->save();
      // Notify the advertiser
      \Drupal::service('plugin.manager.mail')->mail(
        'rds_ad',
        'published',
        $this->node->getOwner()->getEmail(),
        $this->node->getOwner()->getPreferredLangcode(),
        [
          'user' => $this->node->getOwner(),
          'node' => $this->node
        ]
      );
    }
    elseif ($trigger['#value'] == 'Rejeter') {
      $this->node->setSticky(TRUE)
        ->setUnpublished(FALSE)
        ->save();
      $this->mailManager->mail(
        'rds_ad',
        'reject',
        $this->node->getOwner()->getEmail(),
        $this->node->getOwner()->getPreferredLangcode(),
        [
          'user' => $this->node->getOwner(),
          'node' => $this->node,
          'message' => str_replace('[REASON]', $form_state->getValue('message'), SELF::MESSAGE),
        ]
      );
    }
  }

}
