<?php

namespace Drupal\rds_ad;

use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Modify migrations before saving them.
 */
class MigrationSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    //Migrate module might not be installed and we can't test for it here because
    //\Drupal::moduleHandler()->moduleExists('migrate_drupal') is not initialised yet!
    //so just replace the constants with their strings defined in Drupal\migrate\Event\MigrateEvents
    return [
      'migrate.pre_row_save' => ['migratePreRowSave']
    ];
  }

  /**
   * Change the field names
   */
  function migratePreRowSave(MigratePreRowSaveEvent $event) {
    $row = $event->getRow();
    $migration = $event->getMigration();

    if ($migration->id() == 'd7_node:proposition') {
      $row->setDestinationProperty('type', 'annonce');
      $unixtime = $row->getDestinationProperty('field_expires')[0]['value'];
      $row->setDestinationProperty('date', date('Y-m-d', $unixtime));
      $item = $row->getDestinationProperty('field_type_d_annonce');
      switch ($item[0]['target_id']) {
        case 325: $val = 'rh';break;
        case 311: $val = 'ph';break;
        case 319: $val = 'pe';break;
        case 312: $val = 'dg';break;
      }
      $row->setDestinationProperty('annonce_type', [['value' => $val]]);
    }


  }

}
