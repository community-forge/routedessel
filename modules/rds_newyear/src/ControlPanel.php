<?php
namespace Drupal\rds_newyear;

use Drupal\group\Entity\Group;
use Drupal\user\Entity\User;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\State;
use Drupal\Core\Config\Config;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 */
class ControlPanel extends FormBase {

  private Config $settings;
  private Config $userSettings;
  private int $lastStep;
  private State $state;

  function __construct(State $state) {
    $config_factory = $this->configFactory();
    $this->settings = $config_factory->getEditable('rds_newyear.settings');
    $this->userSettings = $config_factory->getEditable('user.mail');
    $this->state = $state;
    $this->lastStep = $state->get('rds_newyear_laststep');
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
    );
  }

  function buildForm($form, FormStateInterface $form_state) {
    $form['intro'] = [
      '#markup' => '<p>'.$this->t('Step through this page to test and enact the new year renewal of memberships.').'</p>',
      "#weight" => 0
    ];
    $correspondents = rds_sel_all_correspondent_uids();
    $benevoles = rds_newyear_benevole_uids();
    $members_only = rds_newyear_member_only_uids();

    $form['step1'] = [
      '#type' => 'details',
      '#title' => $this->t('Step 1: Reward correspondents'),
      '#description' => $this->t('Gives a nuit reward to every correspondent and unpublish ALL lodgings'),
      '#open' => $this->lastStep == 0,
      "#weight" => 1,
      'info' => [
        '#markup' => '<p>'.$this->t('Number of correspondents: '.count($correspondents)).'</p>'
      ],
      'nights_reward' => [
        '#type' => 'number',
        '#title' => $this->t('Number of nights'),
        '#default_value' => $this->settings->get('nights_reward'),
        "#weight" => 2,
      ],
      'nights_reward_description' => [
        '#type' => 'textfield',
        '#title' => $this->t('Description for mass transaction'),
        '#default_value' => $this->settings->get('nights_reward_description'),
        "#weight" => 3,
      ],
      'do1' => [
        '#type' => 'submit',
        '#value' => $this->t('Send reward'),
        '#submit' => ['::sendReward'],
        '#access' => $this->lastStep == 0,
        "#weight" => 5,
      ]
    ];

    $form['step2'] = [
      '#type' => 'details',
      '#title' => $this->t('Step 2: Reset benevoles'),
      '#description' => $this->t("Remove the benevoles' membership role, log them out and notify them."),
      '#open' => $this->lastStep == 1,
      "#weight" => 2,
      'info' => [
        '#markup' => $this->t("@count benevoles (not user 1) will have to pay before they can re-access the site.", ['@count' => count($benevoles)])
      ],
      'reset_benevole_subject' => [
        '#title' => $this->t('Subject'),
        '#type' => 'textfield',
        '#default_value' => $this->settings->get('reset_benevole.subject'),
        "#weight" => 1,
      ],
      'reset_benevole_body' => [
        '#title' => $this->t('Body'),
        '#description' => $this->t('Tokens are allowed.'),
        '#tokentree' => ['user'],
        '#type' => 'textarea',
        '#default_value' => $this->settings->get('reset_benevole.body'),
        "#weight" => 2,
      ],
      'reset_correspondent_body_tokens' => [
        '#theme' => 'token_tree_link',
        '#token_types' => ['user'],
        "#weight" => 3,
      ],
      'test2' => [
        '#type' => 'submit',
        '#value' => $this->t('Test benevole reset'),
        '#submit' => ['::testResetBenevoles'],
        "#weight" => 5,
      ],
      'do2' => [
        '#type' => 'submit',
        '#value' => $this->t('Reset benevoles'),
        '#submit' => ['::resetBenevoles'],
        "#weight" => 5,
        '#access' => $this->lastStep == 1,
      ]
    ];

    $form['step3'] = [
      '#title' => $this->t('Step 3: Reset Members'),
      '#description' => $this->t("Remove adherent role and unverify members, not counting correspondents and benevoles"),
      '#type' => 'details',
      '#open' => $this->lastStep == 2,
      "#weight" => 3,
      'info' => [
        '#markup' => count($members_only) ." paid members will be affected."
      ],
      'user_status_blocked_subject' => [
        '#title' => $this->t('Subject'),
        '#type' => 'textfield',
        '#default_value' => $this->userSettings->get('status_blocked.subject'),
        "#weight" => 1,
      ],
      'user_status_blocked_body' => [
        '#title' => $this->t('Body'),
        '#description' => $this->t('Tokens are allowed.'),
        '#tokentree' => ['user'],
        '#type' => 'textarea',
        '#default_value' => $this->userSettings->get('status_blocked.body'),
        "#weight" => 2,
      ],
      'reset_correspondent_body_tokens' => [
        '#theme' => 'token_tree_link',
        '#token_types' => ['user'],
        "#weight" => 3,
      ],
      'test3' => [
        '#type' => 'submit',
        '#value' => $this->t('Test member reset'),
        '#submit' => ['::testResetMemberMail'],
        "#weight" => 5,
      ],
      'do3' => [
        '#type' => 'submit',
        '#value' => $this->t('Reset members and mail all'),
        '#submit' => ['::resetMembers'],
        "#weight" => 5,
        '#access' => $this->lastStep == 3,
      ]
    ];
    $form['step3a'] = [
      '#type' => 'details',
      '#title' => $this->t("Step 3a: Test the correspondent's verification page"),
      '#open' => $this->lastStep == 2,
      "#weight" => 4,
      'info' => [
        '#markup' => '<p>'.$this->t('Test the verification page for a SEL.').'</p>'
      ],
      'test_sel_verification_form' => [
        '#title' => $this->t('Choose SEL'),
        '#type' => 'entity_autocomplete',
        '#target_type' => 'group',
        '#selection_handler' => 'default:group',
        '#maxlength' => 50,
        '#size' => 40,
        '#placeholder' => 'SEL...',
        "#weight" => 1,
        '#id' => 'sel-chooser' // used by javascript
      ],
      'test_sel_verification_form_link' => [
        '#type' => 'item',
        '#markup' => Markup::create('<a href="" onclick="openVerificationPage();">'.$this->t('Open in a new window...').'</a>'),
        "#weight" => 2,
        '#attached' => ['library' => ['rds_newyear/control-panel']],
        '#states' => [
          'invisible' => [
            ':input[name="test_sel_verification_form"]' => ['value' => '']
          ]
        ]
      ]
    ];
    $form['step4'] = [
      '#type' => 'details',
      '#title' => $this->t('Step 4: Free memberships'),
      '#description' => $this->t("Nominate some users (benevoles) to receive free membership for the coming year"),
      '#open' => $this->lastStep == 3,
      "#weight" => 5,
      'free_member_uids' => [
        '#title' => $this->t('User IDs'),
        '#descripiont' => $this->t('Comma-separated'),
        '#type' => 'textfield',
        '#default_value' => $this->settings->get('free_uids'),
        "#placeholder" => '1, 2558 etc...'
      ],
      'do4' => [
        '#type' => 'submit',
        '#value' => $this->t('Grant free memberships'),
        '#submit' => ['::freeMemberships'],
        "#weight" => 5,
        '#access' => $this->lastStep == 3,
      ]
    ];

    $form['step5'] = [
      '#type' => 'details',
      '#title' => $this->t('Step 5: Reset correspondents'),
      '#description' => $this->t("Remove the correspondents' adherent role, log them out and instruct them about verification."),
      '#open' => $this->lastStep == 4,
      "#weight" => 6,
      'info' => [
        '#markup' => $this->t(
          "@count correspondents will be affected. In addition, a single mail will be sent to the fallback correspondent with links to the @sels_sans SELs which have no correspondent.",
          ['@sans_sel' => count(rds_sels_sans_correspondents()), '@count' => count($correspondents)]
        )
      ],
      'reset_correspondent_subject' => [
        '#title' => $this->t('Subject'),
        '#type' => 'textfield',
        '#default_value' => $this->settings->get('reset_correspondent.subject'),
        "#weight" => 1,
      ],
      'reset_correspondent_body' => [
        '#title' => $this->t('Body'),
        '#description' => $this->t('Tokens are allowed. MUST contain the special token, [sel-verifiy-links]'),
        '#tokentree' => ['user'],
        '#type' => 'textarea',
        '#default_value' => $this->settings->get('reset_correspondent.body'),
        "#weight" => 2,
      ],
      'reset_correspondent_body_tokens' => [
        '#theme' => 'token_tree_link',
        '#token_types' => ['user'],
        "#weight" => 3,
      ],
      'reset_correspondent_uid' => [
        '#title' => $this->t('Test with correspondent'),
        '#description' => $this->t('You will receive the mail generated for this correspondent. Leave blank to receive the fallback correspondent mail.'),
        '#type' => 'entity_autocomplete',
        '#target_type' => 'user',
        '#selection_handler' => 'correspondent:user',
        '#maxlength' => 100,
        '#default_value' => $this->state->get('rds_newyear_test_correspondent_uid') ? User::load($this->state->get('rds_newyear_test_correspondent_uid')) :  NULL,
        '#size' => 60,
        '#placeholder' => $this->t('Correspondent name...'),
        "#weight" => 4,
      ],
      'test5' => [
        '#type' => 'submit',
        '#value' => $this->t('Test correspondent reset'),
        '#submit' => ['::testResetCorrespondent'],
        "#weight" => 5,
      ],
      'do5' => [
        '#type' => 'submit',
        '#value' => $this->t('Reset correspondents'),
        '#submit' => ['::resetCorrespondents'],
        "#weight" => 5,
        '#access' => $this->lastStep == 4,
      ]
    ];

    $verified_sel_ids = $this->settings->get('verified_sels');
    $all_sel_ids = \Drupal::entityQuery('group')->accessCheck(FALSE)
      ->execute();
    $unverified_sel_ids = array_diff($all_sel_ids, $verified_sel_ids);
    $rows = [];
    if ($this->lastStep == 4) {
      foreach (Group::loadMultiple($unverified_sel_ids) as $sel) {
        $accounts = rds_sel_correspondents($sel);
        $sel_url = Url::fromRoute('rds_sel.correspondent.verify_list', ['group' => $sel->id()]);
        $account_names = array_map(function ($a){return $a->toLink()->toString();}, $accounts);
        $rows[] = [
          Link::fromTextAndUrl($sel->label(), $sel_url)->toString(),
          Markup::create(implode(', ', $account_names)),
        ];
      }
    }

    $form['step6'] = [
      '#type' => 'details',
      '#title' => $this->t('Step 6: Monitor verification of SELS'),
      '#description' => $this->t("See which of @count SELs remain unverified this year. Every SEL should be verified.", ['@count' => count($all_sel_ids)]),
      '#open' => $this->lastStep == 5,
      '#weight' => 7,
      'info' => [
        '#markup' => $this->t('@count sels remain unverified', ['@count' => count($unverified_sel_ids)]),
        '#weight' => 0
      ],
      'verified_sels' => [
        '#theme' => 'table',
        '#header' => ['SEL', 'Correspondent(s)', ''],
        '#rows' => $rows,
        '#empty' => t('No SELs are waiting to be verified')
      ]
    ];
    return $form;
  }

  function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  function submitForm(array &$form, FormStateInterface $form_state) {
    die('ControlPanelController::submitForm'); // Should never happen.
  }

  /**
   * Reward all correspondents and unpublish all lodgings.
   * @param array $form
   * @param FormStateInterface $form_state
   */
  function sendReward(array $form, FormStateInterface $form_state) {
    $this->settings->set('nights_reward', $form_state->getValue('nights_reward'));
    $this->settings->set('nights_reward_description', $form_state->getValue('nights_reward_description'));
    $this->settings->save();
  ini_set('max_execution_time', '300');

    rds_newyear_reward_nights();
    \Drupal::database()->update('node_field_data')
      ->fields(['status' => 0])
      ->condition('type', 'lodging')
      ->execute();
    $this->state->set('rds_newyear_laststep', 1);
    \Drupal::messenger()->addStatus($this->t('Mass transaction was created; step 1 complete.'));
  }

  /**
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  function testResetBenevoles(array $form, FormStateInterface $form_state) {
    $this->saveBenevoleSettings($form_state);
    $benevole = User::load(\Drupal::currentUser()->id());
    \Drupal::service('plugin.manager.mail')->mail(
      'rds_newyear',
      'reset_benevole',
      $benevole->getEmail(),
      'fr',
      [
        'benevole' => $benevole
      ]
    );
    \Drupal::messenger()->addStatus($this->t('Check your mail'));
  }

  /**
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  function resetBenevoles(array $form, FormStateInterface $form_state) {
    $this->saveBenevoleSettings($form_state);
    rds_newyear_reset_benevoles();
    $this->state->set('rds_newyear_laststep', 2);
    \Drupal::messenger()->addStatus($this->t('Benevoles have been reset; step 2 complete.'));
  }

  /**
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  function testResetMemberMail(array $form, FormStateInterface $form_state) {
    $this->saveMemberBlockedMail($form_state);
    $user = User::load(\Drupal::currentUser()->id());
    \Drupal::service('plugin.manager.mail')->mail(
      'user',
      'status_blocked',
      $user->getEmail(),
      'fr',
      [
        'account' => $user
      ]
    );
  }

  /**
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  function resetMembers(array $form, FormStateInterface $form_state) {
    $this->saveMemberBlockedMail($form_state);
    rds_newyear_suspend_members();
    $this->state->set('rds_newyear_laststep', 3);
    \Drupal::messenger()->addStatus($this->t('Members have been reset; step 3 complete.'));
  }

  /**
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  function freeMemberships(array $form, FormStateInterface $form_state) {
    $uids = $form_state->getValue('free_member_uids');
    foreach (User::loadMultipe($uids) as $account) {
      rds_payment_grant_free($account);
    }
    \Drupal::messenger()->addWarning('This function has not been tested. Check that the users actually have the memberships...');
  }

  /**
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  function testResetCorrespondent(array $form, FormStateInterface $form_state) {
    $this->saveResetCorrespondent($form_state);
    $uid = $form_state->getValue('reset_correspondent_uid');
    \Drupal::service('plugin.manager.mail')->mail(
      'rds_newyear',
      'reset_correspondent',
      \Drupal::currentUser()->getEmail(),
      'fr',
      [
        'correspondent' => $uid ? User::load($uid) : NULL,
        'recipient' => User::load(\Drupal::currentUser()->id())
      ]
    );
  }

  /**
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  function resetCorrespondents($form, $form_state)  {
    $this->saveResetCorrespondent($form_state);
    rds_newyear_reset_correspondents();
    $this->state->set('rds_newyear_laststep', 4);
    \Drupal::messenger()->addStatus($this->t('Correspondents have been reset and notified; step 4 complete.'));
  }

  /**
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  private function saveResetCorrespondent($form_state) {
    $this->settings->set('reset_correspondent.subject', $form_state->getValue('reset_correspondent_subject'));
    $this->settings->set('reset_correspondent.body', $form_state->getValue('reset_correspondent_body'));
    $this->settings->save();
    $this->state->set('rds_newyear_test_correspondent_uid', $form_state->getValue('reset_correspondent_uid'));
  }

  /**
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  private function saveMemberBlockedMail($form_state) {
    $this->userSettings->set('status_blocked.subject', $form_state->getValue('user_status_blocked_subject'));
    $this->userSettings->set('status_blocked.body', $form_state->getValue('user_status_blocked_body'));
    $this->userSettings->save();
  }

  /**
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  private function saveBenevoleSettings($form_state) {
    $this->settings->set('reset_benevole.subject', $form_state->getValue('reset_benevole_subject'));
    $this->settings->set('reset_benevole.body', $form_state->getValue('reset_benevole_body'));
    $this->settings->save();
  }


  public function getFormId(): string {
    return 'new_year_controller';
  }

}
