<?php

namespace Drupal\rds_newyear\Plugin\QueueWorker;

use Drupal\user\Entity\User;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Entity\Query\Sql\Query;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Attribute\QueueWorker;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Queue worker to clean up redundant users and their content.
 */
#[QueueWorker(
  id: 'rds_remove_user',
  title: new TranslatableMarkup('Delete a user and its lodging nodes.'),
  cron: ['time' => 10]
)]
class RemoveUser extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   *
   * @var Query
   */
  protected $nodeQuery;

 /**
  * constructor
  */
  public function __construct($configuration, $plugin_id, $plugin_definition, EntityTypeManager $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->nodeQuery = $entity_type_manager->getStorage('node')->getQuery()->accessCheck(TRUE);
  }

 /**
  * {@inheritdoc}
  */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

 /**
  * @param int $uid
  */
  public function processItem($uid) {
    $nids = $this->nodeQuery
      ->condition('type', 'lodging')
      ->condition('landlord', $uid)
      ->execute();
    // A user who never paid should not be able to have any lodgings...
    foreach (Node::loadMultiple($nids) as $node) {
      $node->delete();
    }
    User::load($uid)->delete();
  }
}
