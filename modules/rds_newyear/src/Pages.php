<?php
namespace Drupal\rds_newyear;

use Drupal\Core\Controller\ControllerBase;
use \Drupal\user\UserInterface;
/**
 *
 */
class Pages extends ControllerBase {

  /**
   * Expire a single user, notify them, and notify the correspondent.
   * @param \Drupal\user\UserInterface $user
   */
  function ExpireUser(UserInterface $user) {
    $user->removeRole(RID_ADHERENT);
    $user->block()->save(); // this will mail the user.
    $sels = get_sels_of_user($user, TRUE);
    \Drupal::service('plugin.manager.mail')->mail(
      'user',
      'register_pending_approval_admin',
      $user->getEmail(),
      'en',
      [
        'account' => $user,
        'sel' => reset($sels)
      ]
    );
    $this->messenger()->addStatus('Le member '.$user->getDisplayName() . ' a expiré');
    return $this->redirect('entity.user.canonical', ['user' => $user->id()]);
  }
}
