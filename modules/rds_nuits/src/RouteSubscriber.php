<?php

namespace Drupal\rds_nuits;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // In MC5 instead of anonsign theres another path to the confirmation form with the feedback.
    // Seems to work now without this
//    $collection->get('mcapi.signatures.anonsign')
//      ->setPath('/transaction/{mc_transaction}/sign/{uid}/{hash}')
//      ->setDefaults([
//        '_entity_form' => 'mc_transaction.sign'
//      ]);

    // Set the title of all default transaction forms.
//    $collection->get('entity.mc_transaction.add_form')
//      ->setDefault('_title', 'Enregistrer les nuitées')
//      ->setDefault('_title_callback', '');
  }
}
