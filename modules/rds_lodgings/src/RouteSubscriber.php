<?php

namespace Drupal\rds_lodgings;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Alter routes.
 *
 * Change some routes into admin routes. Change some route titles.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // the 'my lodgings' page should only appear to members with lodgings.
    if ($r = $collection->get('view.lodgings_list.my_lodgings_page')) {
      $r->setRequirement('_has_lodgings',  'TRUE');
    }
    else {
      \Drupal::logger('rds')->error('Unable to find route view.lodgings_list.my_lodgings_page in Drupal\rds_lodgings\RouteSubscriber');
    }
  }

}
