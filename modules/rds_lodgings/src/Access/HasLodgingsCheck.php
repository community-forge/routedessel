<?php

namespace Drupal\rds_lodgings\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access to routes based on whether the current user has lodgings listed
 */
class HaslodgingsCheck implements AccessInterface {

  /**
   * Grant access if the user is a member of one and only one SEL
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   * @see rds_lodgings_node_presave();
   */
  public function access(AccountInterface $account, Route $route) {
    return AccessResult::allowedIf(is_landlord($account->id()))->cachePerUser();
  }

}
