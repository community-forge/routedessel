<?php

namespace Drupal\rds_lodgings\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user\Entity\User;

class MyLodgings extends MenuLinkDefault implements ContainerFactoryPluginInterface {

  private $currentUser;

  /**
   *
   * @param $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param $static_override
   * @param $current_user
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, $static_override, $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $static_override);
    $this->currentUser = User::load($current_user->id());
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu_link.static.overrides'),
      $container->get('current_user')
    );
  }

  public function getRouteParameters() {
    return [
      'arg_0' => $this->currentUser->id()
    ];
  }
}
