<?php

namespace Drupal\rds_lodgings\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Display the lodgings flags as icons with css.
 */
#[FieldFormatter(
  id: 'lodgings_flags',
  label: new TranslatableMarkup('Lodgings flags'),
  field_types: ['list_string']
)]
class MiscFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [
      '#attached' => ['library' => ['rds_lodgings/lodgings']],
      '#markup' => ''
    ];
    $map = $items->getFieldDefinition()->getSetting('allowed_values');
    foreach ($items as $delta => $item) {
      $key = $item->value;
      $title = $map[$key];
      $elements['#markup'] .= "<div class=\"lodging-icon icon-$key\" title = \"$title\"></div>";
    }
    return $elements;
  }


}
