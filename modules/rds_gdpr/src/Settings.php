<?php

namespace Drupal\rds_gdpr;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form builder for settings.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rds_gdpr_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildform($form, $form_state);
    $gdpr_settings = $this->config('rds_gdpr.settings');
    foreach (gdpr_get_volunteers() as $uid => $account) {
      $options[$uid] = $account->getDisplayName();
    }
    $form['gdpr_treasurer'] = array(
      '#title' => $this->t('Trésorier'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $gdpr_settings->get('treasurer'),
      '#weight' => 2
    );
    $form['gdpr_comms'] = array(
      '#title' => $this->t('Communication channels used'),
      '#type' => 'checkboxes',
      '#options' => rds_gdpr_comms(),
      '#default_value' => (array)$gdpr_settings->get('comms'),
      '#weight' => 4,
      'cforge_mail' => [
        '#value' => TRUE,
        '#disabled' => TRUE
      ]
    );
    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function __validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::configFactory()->getEditable('rds_gdpr.settings')
      ->set('treasurer', $form_state->getValue('treasurer'))
      ->set('comms', array_filter($form_state->getValue('gdpr_comms')))
      ->set('editor', \Drupal::currentUser()->id())
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['rds_gdpr.settings'];
  }

}
