<?php
namespace Drupal\rds_gdpr;

use Drupal\Core\Controller\ControllerBase;
use \Drupal\Core\StringTranslation\TranslatableMarkup;

class GDPRController extends ControllerBase {

  public function Details() : array {
    $config = \Drupal::config('rds_gdpr.settings');
    $rds = [];
    foreach(gdpr_get_volunteers() as $account) {
      // for some reason toString returns an object
      $link = (string)$account->toLink()->toString();
      $rds[$link] = 'Benevole';
    }

    $cforge = [
      '<a href="http://matslats.net">Matthew Slater</a>' => 'software developeur',
      '<a href="https://slowfarm.co.nz">Phil Stevens</a>' => 'system administrateur',
    ];
    return [
      '#theme' => 'gdpr',
      '#treasurer' => $config->get('treasurer')??1,
      '#channels' => $config->get('comms'),
      '#responsible' => [
        'Route des SEL'  => $rds??[],
        'Community Forge' => $cforge
      ],
      '#editor' => $config->get('editor')??1,
    ];
  }

}
