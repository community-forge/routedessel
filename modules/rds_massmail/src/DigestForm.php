<?php

namespace Drupal\rds_massmail;

use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Core\Entity\Query\Sql\Query;
use Drupal\Core\Mail\MailManagerInterface;

/**
 * Form builder for settings.
 */
class DigestForm extends FormBase {

  /**
   * @var Drupal\Core\Mail\MailManagerInterface
   */
  private $mailManager;

  function __construct(MailManagerInterface $mail_manager) {
    $this->mailManager = $mail_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.mail'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rds_digest';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $annonces = $this->nodeQuery()->accessCheck(TRUE)
      ->condition('promote', TRUE)
      ->condition('status', 1)
      ->condition('type', 'annonce')
      ->sort('annonce_type')
      ->sort('created', 'desc')
      ->execute();
    // Get all the promoted nodes and put them in a table with checkboxes
    $nids = $this->nodeQuery()->accessCheck(TRUE)
      ->condition('promote', TRUE)
      ->condition('status', 1)
      ->condition('type', 'annonce', '<>')
      ->sort('type')
      ->sort('created', 'desc')
      ->execute();

    if (empty($annonces) and empty($nids)) {
      return [
        '#markup' => 'There is no content queued for the next digest'
      ];
    }

    $form['message'] = [
      '#title' => 'Cher [user:name]',
      '#type' => 'textarea',
      '#rows' => 5,
      '#default_value' => "Cliquer sur les titres soulignés (de couleur bleue) pour accéder à l'annonce. Vous trouverez le nom de l'adhérent à la fin de l'annonce, ce qui permet d'accéder à ses coordonnées pour lui téléphoner ou lui envoyer un message via 'contact'.",
      '#required' => FALSE,
      '#weight' => 1,
    ];
    $form['annonces'] = [
      '#type' => 'tableselect',
      '#caption' => 'Enlever la coche pour ne pas envoyer les annonces',
      '#header' => ["Type", "Créé", "Titre"],
      '#options' => [],
      '#empty' => "Il n'y a pas des annonces en attente de diffusion",
      '#weight' => 2
    ];
    $date_service = \Drupal::service('date.formatter');
    foreach (Node::loadMultiple($annonces) as $annonce) {
      $form['annonces']['#options'][$annonce->id()] = [[
        'type' => $this->annonceType($annonce),
        'created' => $date_service->format($annonce->getCreatedTime(), 'custom', 'D jS M Y'),
        'title' => $annonce->toLink()->toString()
      ]];
      $form['annonces']['#value'][$annonce->id()] = 1;
    }

    $form['nodes'] = [
      '#type' => 'tableselect',
      '#caption' => 'Autre nouveau contenu',
      '#header' => ["Type", "Créé", "Titre"],
      '#options' => [],
      '#empty' => "Il n'y a pas de contenu en attente de diffusion",
      '#weight' => 2
    ];

    foreach (Node::loadMultiple($nids) as $node) {
      $form['nodes']['#options'][$node->id()] = [[
        'type' => $node->type->entity->label(),
        'created' => $date_service->format($node->getCreatedTime(), 'custom', 'D jS M Y'),
        'title' => $node->toLink()->toString()
      ]];
      $form['nodes']['#default_value'][$node->id()] = 1;
    }

    $form['preview'] = [
      '#type' => 'submit',
      '#value' => 'Envoyez-moi un mail de test',
      '#weight' => 10,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Envoyer à tous les adhérents',
      '#weight' => 11,
    ];
    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $annonce_nids= array_keys(array_filter($form_state->getValue('annonces')));
    $other_nids = array_keys(array_filter($form_state->getValue('nodes')));
    $body = $this->messageBody($form_state->getValue('message'), $annonce_nids, $other_nids);
    if ($trigger['#id'] == 'edit-preview') {
      $uids = [$this->currentuser()->id()];
      $form_state->setRebuild();
    }
    elseif ($trigger['#id'] == 'edit-submit') {
      // send to all members
      $all_members = \Drupal::EntityQuery('user')->accessCheck(TRUE)
        ->condition('roles', RID_ADHERENT)
        ->condition('status', TRUE)
        ->execute();
      $opted_in = rds_massmail_subscribed('digest');
      $uids = array_intersect($all_members, $opted_in);
      $this->unpromoteAll(array_keys($form_state->getValue('annonces')));
      $this->unpromoteAll(array_keys($form_state->getValue('nodes')));
    }
    else {
      \Drupal::messenger()->addError('There is a bug with form. Please contact matslats.');
      \Drupal::logger('rds')->error('No uids for digest form @$trigger', ['@trigger' => print_r($trigger, 1)]);
      return;
    }
    \Drupal::logger('rds')->notice(
      'Queuing digest notes for @count opted-in users: @nids',
      ['@count' => count($uids), '@nids' => array_merge($annonce_nids, $other_nids)]
    );
    rds_massmail_make_queue('rds_massmail_digest', $uids, ['body' => $body]);

  }

  /**
   *
   * @param array $nids
   * @return void
   */
  function unpromoteAll(array $nids) : void {
    foreach (Node::loadMultiple($nids) as $node) {
      $node->setPromoted(FALSE)->save();
    }
  }

  /**
   *
   * @param string $leader
   * @param array $annonce_nids
   * @param array $other_nids
   * @return string[]
   */
  function messageBody(string $leader, array $annonce_nids, array $other_nids = []) : array {
    $message[] = $leader;
    // Annonces and other nodes are sorted by type from the form, above.
    $type = '';
    foreach (Node::loadMultiple($annonce_nids) as $annonce) {
      if ($this->annonceType($annonce) <> $type) {
        $type = $this->annonceType($annonce);
        $message[] = ucfirst($type) .' :';
      }
      // bullet points without html
      $message[] = ' - '.$annonce->toLink()->toString();
    }
    foreach (Node::loadMultiple($other_nids) as $node) {
      if ($node->type->target_id <> $type) {
        $type = $node->type->target_id;
        $message[] = $type;
      }
      $message[] = ' - ' .$node->toLink()->toString();
    }
    return $message;
  }

  /**
   *
   * @staticvar array $map
   * @param NodeInterface $annonce
   * @return type
   */
  private function annonceType(NodeInterface $annonce) {
    static $map = [];
    if (empty($map)) {
      $map = FieldStorageConfig::load('node.annonce_type')->getSetting('allowed_values');
    }
    return $map[$annonce->annonce_type->value];
  }

  /**
   *
   * @return EntityQuery
   */
  private function nodeQuery() : Query{
    // If I inject this it doesn't appear when the form is rebuilt.
    return \Drupal::entityTypeManager()->getStorage('node')->getQuery()->accessCheck();
  }
}
