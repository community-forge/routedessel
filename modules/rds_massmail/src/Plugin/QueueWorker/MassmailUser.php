<?php

namespace Drupal\rds_massmail\Plugin\QueueWorker;

use Drupal\user\Entity\User;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Mail\MailManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Attribute\QueueWorker;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Queueworker for mass mail to users.
 */
#[QueueWorker(
  id: 'rds_massmail_user',
  title: new TranslatableMarkup('Send one of many massmails to users'),
  cron: ['time' => 25]
)]
class MassmailUser extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   *
   * @var MailManager
   */
  protected $mailManager;

 /**
  * constructor
  */
  public function __construct($configuration, $plugin_id, $plugin_definition, MailManager $mailManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mailManager = $mailManager;
  }

 /**
  * {@inheritdoc}
  */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.mail')
    );
  }

 /**
  * @param int $uid
  * @param stdClass $data
  *   // Object with uid, subject, body, files
  */
  public function processItem($data) {
    $user = User::load($data->uid);
    \Drupal::service('plugin.manager.mail')->mail(
      'rds_massmail',
      'massmail',
      $user->getEmail(),
      $user->getPreferredLangcode(),
      ['user' => $user] + (array)$data,
      FALSE
    );
  }
}
