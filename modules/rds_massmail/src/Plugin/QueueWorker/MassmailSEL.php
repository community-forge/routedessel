<?php

namespace Drupal\rds_massmail\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Mail\MailManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Attribute\QueueWorker;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Queue worker for mass mail to SEL admins
 */
#[QueueWorker(
  id: 'rds_massmail_sel',
  title: new TranslatableMarkup('Send one of many mails to SELs'),
  cron: ['time' => 25]
)]
class MassmailSEL extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   *
   * @var MailManager
   */
  protected $mailManager;

 /**
  * constructor
  */
  public function __construct($configuration, $plugin_id, $plugin_definition, MailManager $mailManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mailManager = $mailManager;
  }

 /**
  * {@inheritdoc}
  */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.mail')
    );
  }

 /**
  * @inheritdoc
  */
  public function processItem($data) {
    // NB data uid is actually a group id.
    \Drupal::service('plugin.manager.mail')->mail(
      'rds_massmail',
      'massmail',
      Group::load($data->uid)->courriel->value,
      'fr',// Groups don't have a language property, nobody is translating, & vast majority are French,
      (array)$data
    );
  }
}
