<?php

namespace Drupal\rds_massmail\Plugin\QueueWorker;

use Drupal\user\Entity\User;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Mail\MailManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Attribute\QueueWorker;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Queue worker for digest mails.
 */
#[QueueWorker(
  id: 'rds_massmail_digest',
  title: new TranslatableMarkup('Send one of many digest mails to users'),
  cron: ['time' => 25]
)]
class MassmailDigest extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   *
   * @var MailManager
   */
  protected $mailManager;

 /**
  * constructor
  */
  public function __construct($configuration, $plugin_id, $plugin_definition, MailManager $mailManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mailManager = $mailManager;
  }

 /**
  * {@inheritdoc}
  */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.mail')
    );
  }

 /**
  * {@inheritdoc}
  */
  public function processItem($data) {
    $user = User::load($data->uid);
    \Drupal::service('plugin.manager.mail')->mail(
      'rds_massmail',
      'digest',
      $user->getEmail(),
      $user->getPreferredLangcode(),
      ['user' => $user, 'body' => $data->body],
      FALSE
    );
  }
}
