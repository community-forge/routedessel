<?php

namespace Drupal\rds_massmail;

use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Modify migrations before saving them.
 *
 * @todo inject user_data, entityquery, configFactory, Database
 */
class MigrationSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      'migrate.pre_row_save' => ['migratePreRowSave']
    ];
  }


  /**
   * Change the field names
   */
  function migratePreRowSave(MigratePreRowSaveEvent $event) {
    if ($event->getMigration()->id() == 'd7_user') {
      $row = $event->getRow();
      $val = $row->getSourceProperty('field_digest')[0]['value'];
      \Drupal::service('user.data')->set(
        'rds_massmail',
        $row->getSourceProperty('uid'),
        'digest',
        $val
      );
      // also field_broadcast?
    }
  }

}
