<?php

namespace Drupal\rds_massmail;

use Drupal\user\Entity\User;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access to routes based on login status of current user.
 */
class UnsubscribeAccess implements AccessInterface {

  /**
   * Grant access if the user is a member of one and only one SEL
   * Route parameters are
   *   user
   *   hash
   *   timestamp
   *   key // newsletter or digest
   * )
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, Route $route) {
    $routematch = \Drupal::routeMatch();
    $hash = $routematch->getParameter('hash');
    if ($hash == user_pass_rehash(User::load($account->id()), $routematch->getParameter('timestamp'))) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

}

