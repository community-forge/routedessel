<?php

namespace Drupal\rds_massmail;

use Drupal\token\TokenInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form builder for settings.
 */
class MassMailForm extends FormBase {

  /**
   * @var Drupal\Core\Mail\MailManagerInterface
   */
  private $mailManager;

  /**
   * @var Drupal\token\TokenInterface
   */
  private $tokenService;


  function __construct(MailManagerInterface $mail_manager, TokenInterface $token_service) {
    $this->mailManager = $mail_manager;
    $this->tokenService = $token_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.mail'),
      $container->get('token'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rds_massmail';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['subject'] = [
      '#title' => 'Sujet',
      '#type' => 'textfield',
      '#weight' => 0,
      '#required' => 1
    ];
    $form['body'] = [
      '#title' => 'Bonjour [user:name]',
      '#description' => $this->config('routedessel.settings')->get('mail_footer'),
      '#type' => 'textarea',
      '#rows' => 5,
      '#required' => TRUE,
      '#weight' => 1,
    ];
    $form['attachment'] = [
      '#title' => 'Pièce jointe',
      '#description' => 'N.B. Les fichiers doivent être aussi petits que possible. Les fichiers plus volumineux pourraient causer des problèmes.',
      '#type' => 'file',
      '#multiple' => TRUE,
      '#required' => FALSE,
      '#weight' => 2,
      '#upload_validators' => [
        'file_validate_is_image' => [],
      ],
    ];

    $form['recipients'] = [
      '#type' => 'select',
      '#title' => 'Envoyez à',
      '#options' => [
        'Personnes' => [
          'myself' => 'Moi-même (test)',
          'members' => 'Les adhérents',
          'benevoles' => 'Les benevoles',
          'correspondents' => 'Les correspondants',
          'blocked' => 'Les anciens membres',
          'hosts' => 'Les hébergeurs',
          'non-hosts' => 'Non hébergeurs',
        ],
        'SELs' => [
          'actifsels' => 'Les SEL actif',
          'toussels' => 'Les SEL connus'
        ]
      ],
      '#default_value' => 'myself'
    ];
    foreach ($form['recipients']['#options'] as $type => &$options) {
      foreach ($options as $key => &$label) {
        $count = rds_massmail_get_recipients($key, TRUE);
        $label .= ' ('.$count.')';
      }
    }
    // Prepend one's own SEL for testing
    if ($mysels = get_sels_of_user(\Drupal::currentUser())) {
      $form['recipients']['#options']['SELs'] =
        ['monsel' => reset($mysels).' (Test)'] +
        $form['recipients']['#options']['SELs'];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Envoyez',
      '#weight' => 15,
    ];
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Check for a new uploaded favicon.
    if (isset($form['attachment'])) {
      $file = _file_save_upload_from_form($form['attachment'], $form_state, 0);
      if ($file) {
        // Put the temporary file in form_values so we can save it on submit.
        $form_state->setValue('attachment', $file);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $ids = rds_massmail_get_recipients($form_state->getValue('recipients'));
    if (count($ids) < 2) {
      $form_state->setRebuild();
      $this->messenger()->addStatus('Sending test mail');
    }
    $params = [
      'subject' => $form_state->getValue('subject'),
      'body' => $form_state->getValue('body'),
      'files' => $form_state->getValue('attachment') ? [$form_state->getValue('attachment')] : []
    ];

    if (in_array($form_state->getValue('recipients'), ['actifsels', 'toussels', 'monsel'])) {
      $queue_name = 'rds_massmail_sel';
      $var = 'gid';
//      foreach (array_chunk($ids, 100) as $chunk) {
//        $batch_def['operations'][] = [
//          'rds_massmail_batch_send_sels',
//          ['rds_massmail', 'massmail', $chunk, $params]
//        ];
//      }
//      batch_set($batch_def);
    }
    else {
      $queue_name = 'rds_massmail_user';
      $var = 'uid';
//      $queue = \Drupal::service('queue')->get('rds_massmail_user');
//      foreach ($ids as $uid) {
//        $queue->createItem((object)compact($uid, $params));
//      }
//      rds_process_queue('rds_massmail_user');

//      rds_massmail_batch_set(
//        'rds_massmail',
//        'massmail',
//        $ids,
//        $params,
//        strtr ('Sending @count mails', ['@count' => count($ids)])
//      );
    }


    rds_massmail_make_queue($queue_name, $ids, $params);

    if ($form_state->getValue('recipients') <> 'myself') {
      $form_state->setRedirect('<front>');
    }
  }

}
