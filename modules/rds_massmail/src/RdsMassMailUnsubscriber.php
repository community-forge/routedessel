<?php

namespace Drupal\rds_massmail;

use Drupal\Core\Controller\ControllerBase;

/**
 * Unsubscribe a user from the digest or newsletter
 */
class RdsMassMailUnsubscriber extends ControllerBase {

  public function Unsubscribe($user, $timestamp, $hash, $key) {
    \Drupal::service('user.data')->set('rds_massmail', $user, $key, 0);
    $this->messenger()->addStatus("Vous avez été désabonné des e-mails de '$key'.");
    return $this->redirect('entity.user.canonical', ['user' => $user]);
  }



}
